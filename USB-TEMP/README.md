# USB-TEMP

USB-TEMP is a small project which utilizes the MCP2221
USB to I2C and UART bridge and exposes it's functionalities.

Along with this there is also a SE95 Temperature sensor on
the I2C Bus which also has it's functionalities exposed
