# ELC (Embedded Linux Computer)

# Hardware

This was both my first high speed design as well as my first linux ready design, so a lot of the hardware is new to me, as well as high speed design techniques.

Kicad was the only one I had used before, so it was the obvious choice, that being said there are a lot of improvements, things such as the differential Pair routing didn't work (might have been a PEBKAC) and having to switch between OpenGL and regular modes to get the length matching feature is less than desirable.

## Component selection

###### Processor

The AT91SAM9260 was chosen because its solderable (ie. not bga) and at the time of component selection i was still following along with the article from [Henrik's blog](http://hforsten.com/making-embedded-linux-computer.html) on building a simple linux embedded device, so only Atmel devices were considered (Today I would probably use a PIC32 MZ just because of price). Using the Atmel chip was not a bad decision, Atmel releases amazing app notes and great documentation on the eval boards (Now is a good time to say that most of my design was ripped from the AT91SAM9260EK eval board).

###### RAM

The RAM decision was original 64MB but I then decided that was not enough so for most of the life cycle it was 128MB but now im considering if its worth shelling out the extra money for a 256MB chip, however this does not affect the PCB design as they should be drop in compatible without any hardware changes.
All the decisions were made due to price (64MB was cheap, 128 was about 5 $ and 256 is about 10 $ per chip).

###### NAND

Choosing the NAND chip was the easiest decision of all of them, just pick a decent size and a lot of them come in an easy to solder format (ie. not BGA), 2GB should be enough for my applications and if nothing else SD card goes up to a lot.

###### I/O

I/O (I2C, SPI, USART, UART, USB host/device, SD Card, User switch, User LED) were all decided after the main routing was done, I tried fitting as mutch as possible without increasing board size too much.
Something I should note, SD card has 2 output interfaces, one with regular headers and one with a dedicated Micro sd card slot, this was because I couldn't find a regular sd connector that was easy to solder for sale (Raspberry Pi B SD connector is a good example) and so if I fail to solder the micro sd connector I still have the Header option.
All these interfaces follow the SAM9260EK eval board schematic.

###### Power regulators

I have never used these specific power regulators but i followed the datasheet and they should work. The 1.8V regulator was inserted inline with the 3.3V because of power dissipation, and the 3.3V probably has enough current to handle everything.

###### Decoupling

Decoupling is a big unknown in this, i followed the schematics but most of the decoupling caps didn't fit near their power ports, this is something that i will only find out if it works when I test it on actual hardware.


## PCB

The 2 layer decision was made because of cost, the current board is around 20$ (OSHPark), and 4 layers would be double that, with the BOM already being so expensive (~40$) I didn't want to add more on top of that. (One off is not bad, but I'm counting with extra hardware for future revisions)

Because of the 2 layer constraint the problems with having a broken ground layer follow, how much this is going to affect the design, I dont know but there will probably be a follow up on the bottom with speed benchmarks.

Component placement was key, initially I was planning on putting both the RAM and NAND under the processor, this would have worked if I didn't have to length match **EVERY SINGLE TRACE**, so the NAND was deemed less important and placed on the bottom of the board.

Notable issues are the length of the RAM lines witch are all length matched to +/- 0.1mm of the SDCLK line (The actual recommendation is +/- 2.54mm) but the DMQH line is 9.8mm longer than that(We'll see how that goes), other concerns are:

* None of the lines have termination resistors (NAND and RAM)
* No impedance matching (To be honest I dont even know how to go about this)(NAND and RAM)
* All of them go trough a via (they tell you not to do that in every app note)(RAM only)
* All the other high speed design guidelines have pretty much been broken everywhere

## Other design decisions

###### JTAG

I chose not to waste space on a JTAG Header, I'm almost sure that I will regret this.

###### Jumpers, Crystals and Boot Order

There are 2 jumpers
The OSCSEL Jumper is kind of pointless since we have a 32.768KHz crystal connected, but I put it in there mostly to try and boot without it eventually.
If we drop the external Oscillator we loose USB Support due to the 16MHz crystal not being supported without it

The BMS Jumper is used to select Internal boot (ROM) or External boot.

Internal Boot is the primary booting mean, it tries to boot from everything and drops to SAM-BA which basically just waits for USB or DBGU Serial port (Not used) instructions

Heres a table of the Internal boot order.

* USB Firmware Download (Boot If successful)
* SPI DataFlash Boot (NPCS0 and NPCS1 of the SPI0) (Boot if successful)
* NAND Flash boot (Boot if successful)
* SAM-BA Monitor

External Boot is quite a bit more simple, it boots directly to a default Static Memory Controller (Chip Select 0) and the bootloader finishes setting up everything. We don't have anything connected to NCS0 (Chip select 0) and so External boot is useless, the jumper is there because i was blindly following the SAM9260EK Reference board and a jumper that selects boot order seems important.
It's not worth removing the jumper now, maybe in board V1 we could replace it with more important signals, such as DBGU Serial port.

###### Reset circuitry

Was blindley ripped of off the reference implementation. (Should work)

###### Buttons and LED's

There are 2 LED's and 4 Buttons

One LED is hardwired to 5V and has a power indicator function.
The ULED can be driven by the Kernel or if desired used in Userspace applications

For the WKUP and RESET buttons the reference implementation was followed.
The USW can be used for any application.
The SHDN Button is quite an unkown, most of the app notes say to connect to the SHDN pin on the power supply, I don't have any so, following the MCU datasheet a 1.5M pull-up was used.