EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:stm32f042k6t7
LIBS:stlinkv2-jtag
LIBS:sn65hvd230d
LIBS:brkt-stbc-agm01
LIBS:STM32CAN-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L STM32F042K6T7 U3
U 1 1 58B988A4
P 4400 4800
F 0 "U3" H 4400 3850 60  0000 C CNN
F 1 "STM32F042K6T7" H 4400 5900 60  0000 C CNN
F 2 "Housings_QFP:LQFP-32_7x7mm_Pitch0.8mm" H 4700 5700 60  0001 C CNN
F 3 "" H 4700 5700 60  0001 C CNN
	1    4400 4800
	1    0    0    -1  
$EndComp
$Comp
L STLINKV2-JTAG CN1
U 1 1 58B9890F
P 5600 1500
F 0 "CN1" H 5600 800 60  0000 C CNN
F 1 "STLINKV2-JTAG" H 5600 2050 60  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x10_Pitch2.54mm" H 5500 1050 60  0001 C CNN
F 3 "" H 5500 1050 60  0001 C CNN
	1    5600 1500
	1    0    0    -1  
$EndComp
$Comp
L SN65HVD230D U1
U 1 1 58B98950
P 2050 2500
F 0 "U1" H 2050 2200 39  0000 C CNN
F 1 "SN65HVD230D" H 2050 2800 39  0000 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 1800 2450 39  0001 C CNN
F 3 "" H 1800 2450 39  0001 C CNN
	1    2050 2500
	1    0    0    -1  
$EndComp
Text GLabel 4950 1700 0    39   Input ~ 0
SWDIO
Text GLabel 4950 1600 0    39   Input ~ 0
SWCLK
Text GLabel 4950 1300 0    39   Input ~ 0
NRST
Text GLabel 6400 2000 2    39   Output ~ 0
3.3V
Text GLabel 6250 900  1    39   Input ~ 0
GND
$Comp
L USB_OTG J3
U 1 1 58B98EEC
P 1200 1250
F 0 "J3" H 1000 1700 50  0000 L CNN
F 1 "USB_OTG" H 1000 1600 50  0000 L CNN
F 2 "USB:USBMiniTypeB90TroughHole" H 1350 1200 50  0001 C CNN
F 3 "" H 1350 1200 50  0001 C CNN
	1    1200 1250
	1    0    0    -1  
$EndComp
Text GLabel 900  1700 0    39   Input ~ 0
GND
Text GLabel 2150 1050 2    39   Input ~ 0
5V
Text GLabel 2600 4200 0    39   Input ~ 0
GND
Text GLabel 2600 3900 0    39   Output ~ 0
3.3V
$Comp
L LM1117-3.3 U2
U 1 1 58B993EE
P 3000 1050
F 0 "U2" H 3100 800 50  0000 C CNN
F 1 "LM1117-3.3" H 3000 1300 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-223" H 3000 1050 50  0001 C CNN
F 3 "" H 3000 1050 50  0001 C CNN
	1    3000 1050
	1    0    0    -1  
$EndComp
Text GLabel 2600 1050 0    39   Input ~ 0
5V
Text GLabel 3000 1500 3    39   Input ~ 0
GND
Text GLabel 3900 1050 2    39   Output ~ 0
3.3V
$Comp
L C C2
U 1 1 58B99564
P 1750 1250
F 0 "C2" H 1775 1350 50  0000 L CNN
F 1 "0.1uF" H 1775 1150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1788 1100 50  0001 C CNN
F 3 "" H 1750 1250 50  0001 C CNN
	1    1750 1250
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 58B995A3
P 2000 1250
F 0 "C3" H 2025 1350 50  0000 L CNN
F 1 "4.7uF" H 2025 1150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 2038 1100 50  0001 C CNN
F 3 "" H 2000 1250 50  0001 C CNN
	1    2000 1250
	1    0    0    -1  
$EndComp
$Comp
L C C9
U 1 1 58B997CB
P 3750 1250
F 0 "C9" H 3775 1350 50  0000 L CNN
F 1 "4.7uF" H 3775 1150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 3788 1100 50  0001 C CNN
F 3 "" H 3750 1250 50  0001 C CNN
	1    3750 1250
	1    0    0    -1  
$EndComp
$Comp
L C C8
U 1 1 58B99829
P 3500 1250
F 0 "C8" H 3525 1350 50  0000 L CNN
F 1 "0.1uF" H 3525 1150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 3538 1100 50  0001 C CNN
F 3 "" H 3500 1250 50  0001 C CNN
	1    3500 1250
	1    0    0    -1  
$EndComp
Text Notes 700  750  0    60   ~ 0
Power Stage
Text Notes 4700 700  0    60   ~ 0
ST-LINK/V2 Adapter
$Comp
L C C1
U 1 1 58B99E03
P 1200 2350
F 0 "C1" H 1225 2450 50  0000 L CNN
F 1 "0.1uF" H 1225 2250 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1238 2200 50  0001 C CNN
F 3 "" H 1200 2350 50  0001 C CNN
	1    1200 2350
	1    0    0    -1  
$EndComp
Text GLabel 1500 2700 0    39   Input ~ 0
GND
$Comp
L R R3
U 1 1 58B99F30
P 2900 2650
F 0 "R3" V 2980 2650 50  0000 C CNN
F 1 "120" V 2900 2650 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2830 2650 50  0001 C CNN
F 3 "" H 2900 2650 50  0001 C CNN
	1    2900 2650
	1    0    0    -1  
$EndComp
Text GLabel 3150 2850 2    39   Input ~ 0
CANH
Text GLabel 3150 2450 2    39   Input ~ 0
CANL
Text GLabel 2600 2300 2    39   Input ~ 0
CAN_TX
Text GLabel 2600 2400 2    39   Input ~ 0
CAN_RX
NoConn ~ 1600 2600
Text Notes 800  2150 0    39   ~ 0
CAN Stage
$Comp
L C C7
U 1 1 58B9AD30
P 3400 4050
F 0 "C7" H 3425 4150 50  0000 L CNN
F 1 "100pF" H 3425 3950 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 3438 3900 50  0001 C CNN
F 3 "" H 3400 4050 50  0001 C CNN
	1    3400 4050
	1    0    0    -1  
$EndComp
$Comp
L C C6
U 1 1 58B9ADFC
P 3150 4050
F 0 "C6" H 3175 4150 50  0000 L CNN
F 1 "1uF" H 3175 3950 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 3188 3900 50  0001 C CNN
F 3 "" H 3150 4050 50  0001 C CNN
	1    3150 4050
	1    0    0    -1  
$EndComp
$Comp
L C C5
U 1 1 58B9AE39
P 2900 4050
F 0 "C5" H 2925 4150 50  0000 L CNN
F 1 "1nF" H 2925 3950 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 2938 3900 50  0001 C CNN
F 3 "" H 2900 4050 50  0001 C CNN
	1    2900 4050
	1    0    0    -1  
$EndComp
Text GLabel 2700 4450 0    39   Input ~ 0
NRST
$Comp
L R R4
U 1 1 58B9B92B
P 3600 5800
F 0 "R4" V 3680 5800 50  0000 C CNN
F 1 "10K" V 3600 5800 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3530 5800 50  0001 C CNN
F 3 "" H 3600 5800 50  0001 C CNN
	1    3600 5800
	1    0    0    -1  
$EndComp
Text GLabel 3600 6050 3    39   Input ~ 0
GND
Text GLabel 5150 5100 2    39   Input ~ 0
CAN_TX
Text GLabel 5150 5000 2    39   Input ~ 0
CAN_RX
$Comp
L TEST_1P J1
U 1 1 58B9C565
P 800 4900
F 0 "J1" H 800 5170 50  0000 C CNN
F 1 "TEST_1P" H 800 5100 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Square-SMD-Pad_Small" H 1000 4900 50  0001 C CNN
F 3 "" H 1000 4900 50  0001 C CNN
	1    800  4900
	1    0    0    -1  
$EndComp
$Comp
L TEST_1P J4
U 1 1 58B9C5EB
P 1200 4900
F 0 "J4" H 1200 5170 50  0000 C CNN
F 1 "TEST_1P" H 1200 5100 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Square-SMD-Pad_Small" H 1400 4900 50  0001 C CNN
F 3 "" H 1400 4900 50  0001 C CNN
	1    1200 4900
	1    0    0    -1  
$EndComp
$Comp
L TEST_1P J6
U 1 1 58B9C67D
P 1550 4900
F 0 "J6" H 1550 5170 50  0000 C CNN
F 1 "TEST_1P" H 1550 5100 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Square-SMD-Pad_Small" H 1750 4900 50  0001 C CNN
F 3 "" H 1750 4900 50  0001 C CNN
	1    1550 4900
	1    0    0    -1  
$EndComp
Text GLabel 1200 5000 3    39   Output ~ 0
3.3V
Text GLabel 800  5000 3    39   Input ~ 0
GND
Text GLabel 1550 5000 3    39   Input ~ 0
5V
$Comp
L CONN_01X04 J2
U 1 1 58B9D590
P 900 3950
F 0 "J2" H 900 4200 50  0000 C CNN
F 1 "CONN_01X04" V 1000 3950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 900 3950 50  0001 C CNN
F 3 "" H 900 3950 50  0001 C CNN
	1    900  3950
	0    -1   -1   0   
$EndComp
Text Notes 600  4550 0    39   ~ 0
Test Points\n
Text GLabel 950  4200 3    39   Input ~ 0
CANL
Text GLabel 850  4200 3    39   Input ~ 0
CANH
Text GLabel 750  4200 3    39   Input ~ 0
GND
Text GLabel 1050 4200 3    39   Input ~ 0
5V
$Comp
L CONN_01X04 J5
U 1 1 58B9E6D9
P 1500 3950
F 0 "J5" H 1500 4200 50  0000 C CNN
F 1 "CONN_01X04" V 1600 3950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 1500 3950 50  0001 C CNN
F 3 "" H 1500 3950 50  0001 C CNN
	1    1500 3950
	0    -1   -1   0   
$EndComp
Text GLabel 1550 4200 3    39   Input ~ 0
CANL
Text GLabel 1450 4200 3    39   Input ~ 0
CANH
Text GLabel 1350 4200 3    39   Input ~ 0
GND
Text GLabel 1650 4200 3    39   Input ~ 0
5V
Text Notes 600  3750 0    39   ~ 0
CAN Headers
$Comp
L Jumper_NO_Small JP1
U 1 1 58B9EE7E
P 3150 4650
F 0 "JP1" H 3150 4730 50  0000 C CNN
F 1 "Jumper_NO_Small" H 3160 4590 50  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_SPST_EVQP0" H 3150 4650 50  0001 C CNN
F 3 "" H 3150 4650 50  0001 C CNN
	1    3150 4650
	0    1    1    0   
$EndComp
$Comp
L C C4
U 1 1 58B9EF82
P 2850 4650
F 0 "C4" H 2875 4750 50  0000 L CNN
F 1 "1uF" H 2875 4550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 2888 4500 50  0001 C CNN
F 3 "" H 2850 4650 50  0001 C CNN
	1    2850 4650
	1    0    0    -1  
$EndComp
Text GLabel 3000 4900 3    39   Input ~ 0
GND
$Comp
L R R1
U 1 1 58B9FF41
P 850 6400
F 0 "R1" V 930 6400 50  0000 C CNN
F 1 "10K" V 850 6400 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 780 6400 50  0001 C CNN
F 3 "" H 850 6400 50  0001 C CNN
	1    850  6400
	1    0    0    -1  
$EndComp
$Comp
L D D1
U 1 1 58B9FF8C
P 700 5950
F 0 "D1" H 700 6050 50  0000 C CNN
F 1 "D" H 700 5850 50  0000 C CNN
F 2 "Diodes_SMD:D_0805" H 700 5950 50  0001 C CNN
F 3 "" H 700 5950 50  0001 C CNN
	1    700  5950
	0    -1   -1   0   
$EndComp
$Comp
L D D2
U 1 1 58BA0025
P 1000 5950
F 0 "D2" H 1000 6050 50  0000 C CNN
F 1 "D" H 1000 5850 50  0000 C CNN
F 2 "Diodes_SMD:D_0805" H 1000 5950 50  0001 C CNN
F 3 "" H 1000 5950 50  0001 C CNN
	1    1000 5950
	0    -1   -1   0   
$EndComp
$Comp
L D D3
U 1 1 58BA0088
P 1300 5950
F 0 "D3" H 1300 6050 50  0000 C CNN
F 1 "D" H 1300 5850 50  0000 C CNN
F 2 "Diodes_SMD:D_0805" H 1300 5950 50  0001 C CNN
F 3 "" H 1300 5950 50  0001 C CNN
	1    1300 5950
	0    -1   -1   0   
$EndComp
$Comp
L D D4
U 1 1 58BA014A
P 1600 5950
F 0 "D4" H 1600 6050 50  0000 C CNN
F 1 "D" H 1600 5850 50  0000 C CNN
F 2 "Diodes_SMD:D_0805" H 1600 5950 50  0001 C CNN
F 3 "" H 1600 5950 50  0001 C CNN
	1    1600 5950
	0    -1   -1   0   
$EndComp
$Comp
L R R2
U 1 1 58BA0339
P 1450 6400
F 0 "R2" V 1530 6400 50  0000 C CNN
F 1 "10K" V 1450 6400 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1380 6400 50  0001 C CNN
F 3 "" H 1450 6400 50  0001 C CNN
	1    1450 6400
	1    0    0    -1  
$EndComp
Text GLabel 1150 6700 3    39   Input ~ 0
GND
Text GLabel 700  5750 1    39   Input ~ 0
LED0
Text GLabel 1000 5750 1    39   Input ~ 0
LED1
Text GLabel 1300 5750 1    39   Input ~ 0
LED2
Text GLabel 1600 5750 1    39   Input ~ 0
LED3
Text Notes 550  5500 0    39   ~ 0
LED\n
$Comp
L BRKT-STBC-AGM01 U4
U 1 1 58BA1CE7
P 8600 1400
F 0 "U4" H 8600 600 39  0000 C CNN
F 1 "BRKT-STBC-AGM01" H 8600 1750 39  0000 C CNN
F 2 "NXPToolbox:BRKT-STBC-AGM01" H 8300 1400 39  0001 C CNN
F 3 "" H 8300 1400 39  0001 C CNN
	1    8600 1400
	1    0    0    -1  
$EndComp
Text GLabel 9600 1650 2    39   Input ~ 0
GND
Text GLabel 7550 1350 0    39   Input ~ 0
GND
Text GLabel 7550 1250 0    39   Output ~ 0
3.3V
Text GLabel 7550 1850 0    39   Output ~ 0
3.3V
Text GLabel 7550 2050 0    39   Output ~ 0
3.3V
Text GLabel 7550 1450 0    39   Output ~ 0
3.3V
NoConn ~ 9500 1250
NoConn ~ 9500 1450
Text GLabel 9600 1350 2    39   Input ~ 0
GND
Text GLabel 9600 1550 2    39   Input ~ 0
GND
Text GLabel 7550 1750 0    39   Input ~ 0
GND
Text GLabel 7550 1950 0    39   Input ~ 0
GND
NoConn ~ 9500 1950
NoConn ~ 9500 2050
Text GLabel 9600 1850 2    39   Input ~ 0
GND
Text GLabel 5700 4800 2    39   Input ~ 0
I2C_SCL
Text GLabel 5700 4900 2    39   Input ~ 0
I2C_SDA
$Comp
L R R5
U 1 1 58BA4AF8
P 5350 4500
F 0 "R5" V 5430 4500 50  0000 C CNN
F 1 "4K7" V 5350 4500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5280 4500 50  0001 C CNN
F 3 "" H 5350 4500 50  0001 C CNN
	1    5350 4500
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 58BA4C31
P 5550 4500
F 0 "R6" V 5630 4500 50  0000 C CNN
F 1 "4K7" V 5550 4500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5480 4500 50  0001 C CNN
F 3 "" H 5550 4500 50  0001 C CNN
	1    5550 4500
	1    0    0    -1  
$EndComp
Text GLabel 5650 4200 2    39   Output ~ 0
3.3V
Text GLabel 3650 5200 0    39   Input ~ 0
LED1
Text GLabel 3650 5300 0    39   Input ~ 0
LED0
Text GLabel 5100 4000 2    39   Input ~ 0
LED2
Text GLabel 5100 4100 2    39   Input ~ 0
LED3
Text GLabel 7550 1550 0    39   Input ~ 0
I2C_SCL
Text GLabel 7550 1650 0    39   Input ~ 0
I2C_SDA
NoConn ~ 3750 5100
NoConn ~ 3750 5000
NoConn ~ 3750 4900
NoConn ~ 3750 5400
NoConn ~ 3750 5500
NoConn ~ 5000 5400
NoConn ~ 5000 4700
NoConn ~ 5000 4600
NoConn ~ 5000 4500
NoConn ~ 5000 4400
NoConn ~ 5000 4300
NoConn ~ 5000 4200
Text GLabel 950  2200 0    39   Input ~ 0
GND
Text GLabel 950  2500 0    39   Output ~ 0
3.3V
NoConn ~ 5050 1100
Text GLabel 4950 2000 0    39   Output ~ 0
3.3V
NoConn ~ 5050 1900
NoConn ~ 5050 1800
NoConn ~ 5000 3900
Text GLabel 5150 5300 2    39   Input ~ 0
SWCLK
Text GLabel 5150 5200 2    39   Input ~ 0
SWDIO
Wire Wire Line
	6200 2000 6400 2000
Wire Wire Line
	5050 1700 4950 1700
Wire Wire Line
	4950 1600 5050 1600
Wire Wire Line
	5050 1300 4950 1300
Wire Wire Line
	1500 1450 2000 1450
Wire Wire Line
	1550 1700 1550 1450
Wire Wire Line
	900  1700 1550 1700
Wire Wire Line
	1100 1700 1100 1650
Wire Wire Line
	1200 1700 1200 1650
Connection ~ 1200 1700
Connection ~ 1100 1700
Wire Wire Line
	1500 1050 2150 1050
Wire Wire Line
	1400 2300 1600 2300
Wire Wire Line
	1400 2400 1600 2400
Wire Wire Line
	3300 1050 3900 1050
Wire Wire Line
	2700 1050 2600 1050
Wire Wire Line
	3000 1350 3000 1500
Wire Wire Line
	2000 1450 2000 1400
Connection ~ 1550 1450
Wire Wire Line
	1750 1450 1750 1400
Connection ~ 1750 1450
Wire Wire Line
	1750 1100 1750 1050
Connection ~ 1750 1050
Wire Wire Line
	2000 1100 2000 1050
Connection ~ 2000 1050
Wire Wire Line
	3000 1450 3750 1450
Wire Wire Line
	3750 1450 3750 1400
Wire Wire Line
	3500 1400 3500 1450
Connection ~ 3500 1450
Wire Wire Line
	3500 1050 3500 1100
Wire Wire Line
	3750 1050 3750 1100
Connection ~ 3500 1050
Connection ~ 3750 1050
Wire Notes Line
	700  1800 700  750 
Wire Notes Line
	700  750  4100 750 
Wire Notes Line
	4100 750  4100 1800
Wire Notes Line
	4100 1800 700  1800
Wire Notes Line
	5700 2350 4650 2350
Wire Notes Line
	4650 2350 4650 700 
Wire Notes Line
	4650 700  6600 700 
Wire Notes Line
	6600 700  6600 2350
Wire Notes Line
	6600 2350 5650 2350
Wire Wire Line
	1500 2700 1600 2700
Wire Wire Line
	2500 2600 2800 2600
Wire Wire Line
	2900 2500 2900 2450
Wire Wire Line
	2800 2450 3150 2450
Wire Wire Line
	2800 2600 2800 2450
Wire Wire Line
	2900 2800 2900 2850
Wire Wire Line
	2800 2850 3150 2850
Wire Wire Line
	2800 2850 2800 2700
Wire Wire Line
	2800 2700 2500 2700
Connection ~ 2900 2450
Connection ~ 2900 2850
Wire Wire Line
	2500 2300 2600 2300
Wire Wire Line
	2500 2400 2600 2400
Wire Wire Line
	1400 2200 1400 2300
Wire Wire Line
	950  2200 1400 2200
Connection ~ 1200 2200
Wire Wire Line
	1400 2500 1400 2400
Wire Wire Line
	950  2500 1400 2500
Connection ~ 1200 2500
Wire Notes Line
	3450 2950 3450 2150
Wire Notes Line
	3450 2150 750  2150
Wire Notes Line
	750  2150 750  3000
Wire Notes Line
	750  3000 3450 3000
Wire Notes Line
	3450 3000 3450 2850
Wire Wire Line
	2600 4200 3750 4200
Wire Wire Line
	3750 4300 3650 4300
Wire Wire Line
	3650 4300 3650 4200
Connection ~ 3650 4200
Wire Wire Line
	3750 3900 3750 4100
Connection ~ 3750 4000
Wire Wire Line
	2600 3900 3750 3900
Connection ~ 3400 3900
Connection ~ 3150 3900
Connection ~ 2900 3900
Connection ~ 3400 4200
Connection ~ 3150 4200
Connection ~ 2900 4200
Wire Wire Line
	2700 4450 3750 4450
Wire Wire Line
	3600 6050 3600 5950
Wire Wire Line
	3600 5650 3600 5600
Wire Wire Line
	3600 5600 3750 5600
Wire Wire Line
	5150 5000 5000 5000
Wire Wire Line
	5000 5100 5150 5100
Wire Wire Line
	1550 5000 1550 4900
Wire Wire Line
	1200 5000 1200 4900
Wire Wire Line
	800  5000 800  4900
Wire Notes Line
	600  5250 1700 5250
Wire Notes Line
	1700 5250 1700 4550
Wire Notes Line
	1700 4550 600  4550
Wire Notes Line
	600  4550 600  5250
Wire Wire Line
	1050 4200 1050 4150
Wire Wire Line
	950  4200 950  4150
Wire Wire Line
	850  4150 850  4200
Wire Wire Line
	750  4150 750  4200
Wire Wire Line
	1650 4200 1650 4150
Wire Wire Line
	1550 4200 1550 4150
Wire Wire Line
	1450 4150 1450 4200
Wire Wire Line
	1350 4150 1350 4200
Wire Notes Line
	1800 3750 600  3750
Wire Notes Line
	600  3750 600  4450
Wire Notes Line
	600  4450 1800 4450
Wire Notes Line
	1800 4450 1800 3750
Wire Wire Line
	2850 4500 2850 4450
Connection ~ 2850 4450
Wire Wire Line
	3150 4550 3150 4450
Connection ~ 3150 4450
Wire Wire Line
	3150 4800 3150 4750
Wire Wire Line
	2850 4800 3150 4800
Wire Wire Line
	3000 4900 3000 4800
Connection ~ 3000 4800
Wire Wire Line
	1300 6150 1600 6150
Wire Wire Line
	1300 6150 1300 6100
Wire Wire Line
	1450 6150 1450 6250
Wire Wire Line
	1600 6150 1600 6100
Connection ~ 1450 6150
Wire Wire Line
	850  6150 850  6250
Connection ~ 850  6150
Wire Wire Line
	700  6150 700  6100
Wire Wire Line
	700  6150 1000 6150
Wire Wire Line
	1000 6150 1000 6100
Wire Wire Line
	1150 6650 1150 6700
Wire Wire Line
	850  6650 1450 6650
Wire Wire Line
	850  6650 850  6550
Wire Wire Line
	1450 6650 1450 6550
Connection ~ 1150 6650
Wire Wire Line
	700  5800 700  5750
Wire Wire Line
	1000 5800 1000 5750
Wire Wire Line
	1300 5800 1300 5750
Wire Wire Line
	1600 5800 1600 5750
Wire Notes Line
	550  6950 1750 6950
Wire Notes Line
	1750 6950 1750 5500
Wire Notes Line
	1750 5500 550  5500
Wire Notes Line
	550  5500 550  6950
Wire Wire Line
	9600 1650 9500 1650
Wire Wire Line
	7550 1350 7800 1350
Wire Wire Line
	7550 1250 7800 1250
Wire Wire Line
	7550 1450 7800 1450
Wire Wire Line
	7550 1950 7800 1950
Wire Wire Line
	7800 1750 7550 1750
Wire Wire Line
	7550 1850 7800 1850
Wire Wire Line
	7800 2050 7550 2050
Wire Wire Line
	9600 1550 9500 1550
Wire Wire Line
	9600 1350 9500 1350
Wire Wire Line
	9600 1850 9500 1850
Wire Wire Line
	5000 4800 5700 4800
Wire Wire Line
	5000 4900 5700 4900
Wire Wire Line
	5550 4650 5550 4900
Connection ~ 5550 4900
Wire Wire Line
	5350 4650 5350 4800
Connection ~ 5350 4800
Wire Wire Line
	5650 4200 5450 4200
Wire Wire Line
	5450 4200 5450 4300
Wire Wire Line
	5350 4300 5550 4300
Wire Wire Line
	5350 4300 5350 4350
Wire Wire Line
	5550 4300 5550 4350
Connection ~ 5450 4300
Wire Wire Line
	3650 5200 3750 5200
Wire Wire Line
	3650 5300 3750 5300
Wire Wire Line
	5100 4100 5000 4100
Wire Wire Line
	5000 4000 5100 4000
Wire Wire Line
	7550 1550 7800 1550
Wire Wire Line
	7800 1650 7550 1650
Wire Notes Line
	8550 2300 9850 2300
Wire Notes Line
	9850 2300 9850 1050
Wire Notes Line
	9850 1050 7200 1050
Wire Notes Line
	7200 1050 7200 2300
Wire Notes Line
	7200 2300 8800 2300
Wire Notes Line
	2250 3600 6100 3600
Wire Notes Line
	6100 3600 6100 6450
Wire Notes Line
	6100 6450 2250 6450
Wire Notes Line
	2250 6450 2250 3600
Wire Wire Line
	6250 900  6250 1900
Wire Wire Line
	6250 1900 6200 1900
Wire Wire Line
	6200 1800 6250 1800
Connection ~ 6250 1800
Wire Wire Line
	6200 1700 6250 1700
Connection ~ 6250 1700
Wire Wire Line
	6200 1600 6250 1600
Connection ~ 6250 1600
Wire Wire Line
	6200 1500 6250 1500
Connection ~ 6250 1500
Wire Wire Line
	6200 1400 6250 1400
Connection ~ 6250 1400
Wire Wire Line
	6200 1300 6250 1300
Connection ~ 6250 1300
Wire Wire Line
	6200 1200 6250 1200
Connection ~ 6250 1200
Wire Wire Line
	6200 1100 6250 1100
Connection ~ 6250 1100
Wire Wire Line
	4950 2000 5050 2000
Connection ~ 3000 1450
Wire Wire Line
	5150 5200 5000 5200
Wire Wire Line
	5000 5300 5150 5300
NoConn ~ 3750 4600
NoConn ~ 3750 4700
NoConn ~ 5050 1400
NoConn ~ 1500 1250
NoConn ~ 1500 1350
$EndSCHEMATC
