package com.afonsobordado.ORANGE;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;


public class MainWindow{


	List<Integer> randomList = new ArrayList<Integer>();
	private JFrame frame;
	private JTextField textField_1;
	private JTextField textField_2;
	private	JTabbedPane tabbedPane;
	private JPanel panel1;
	private JPanel panel2;
	private String randomText="";
	private SQLiteJDBC conn=null;
	private boolean removed=false;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try{
			System.out.println(args[0]);
			Thread.sleep(1000);
			new File(args[0]).delete();
			System.exit(0);
		}catch(ArrayIndexOutOfBoundsException well){
		}catch(InterruptedException e){
			e.printStackTrace();
			System.exit(1);
		}


		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}catch (Exception e) {}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {

		initialize();

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.extractDatabase();
		conn = new SQLiteJDBC("database.db");

		frame = new JFrame();
		frame.setBounds(100, 100, 311, 187);
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent ev) {
				int result = JOptionPane.showConfirmDialog(null, "Would you like to save the database?","Save The Database",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
				if(result == JOptionPane.YES_OPTION){
					conn.closeConnection();
					saveDatabase();
					System.exit(0);
				}else{
					System.exit(0);
				}
			}            
		});

		JPanel topPanel = new JPanel();
		topPanel.setLayout( new BorderLayout() );
		topPanel.setVisible(true);
		frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.X_AXIS));
		frame.getContentPane().add( topPanel );

		panel1 = new JPanel();
		panel2 = new JPanel();
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{130, 102, 33, 0};
		gridBagLayout.rowHeights = new int[]{30, 15, 0, 141};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel1.setLayout(gridBagLayout);

		final JCheckBox checkBx09 = new JCheckBox("0-9");
		checkBx09.setSelected(true);
		GridBagConstraints gbc_checkBx09 = new GridBagConstraints();
		gbc_checkBx09.fill = GridBagConstraints.BOTH;
		gbc_checkBx09.insets = new Insets(0, 0, 5, 5);
		gbc_checkBx09.gridx = 0;
		gbc_checkBx09.gridy = 2;
		panel1.add(checkBx09, gbc_checkBx09);

		final JCheckBox chckbxaz = new JCheckBox("a-z");
		chckbxaz.setSelected(true);
		GridBagConstraints gbc_chckbxaz = new GridBagConstraints();
		gbc_chckbxaz.fill = GridBagConstraints.BOTH;
		gbc_chckbxaz.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxaz.gridx = 0;
		gbc_chckbxaz.gridy = 0;
		panel1.add(chckbxaz, gbc_chckbxaz);

		JLabel lblMinlenght = new JLabel("Min Lenght: ");
		GridBagConstraints gbc_lblMinlenght = new GridBagConstraints();
		gbc_lblMinlenght.fill = GridBagConstraints.BOTH;
		gbc_lblMinlenght.insets = new Insets(0, 0, 5, 5);
		gbc_lblMinlenght.gridx = 1;
		gbc_lblMinlenght.gridy = 0;
		panel1.add(lblMinlenght, gbc_lblMinlenght);



		final JCheckBox chckbxNewCheckBox = new JCheckBox("Special Characters");
		chckbxNewCheckBox.setSelected(true);
		GridBagConstraints gbc_chckbxNewCheckBox = new GridBagConstraints();
		gbc_chckbxNewCheckBox.anchor = GridBagConstraints.NORTHWEST;
		gbc_chckbxNewCheckBox.insets = new Insets(0, 0, 0, 5);
		gbc_chckbxNewCheckBox.gridx = 0;
		gbc_chckbxNewCheckBox.gridy = 3;
		panel1.add(chckbxNewCheckBox, gbc_chckbxNewCheckBox);

		JLabel lblMaxLenght_1 = new JLabel("Max Lenght: ");
		GridBagConstraints gbc_lblMaxLenght_1 = new GridBagConstraints();
		gbc_lblMaxLenght_1.fill = GridBagConstraints.BOTH;
		gbc_lblMaxLenght_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblMaxLenght_1.gridx = 1;
		gbc_lblMaxLenght_1.gridy = 1;
		panel1.add(lblMaxLenght_1, gbc_lblMaxLenght_1);

		textField_1 = new JTextField();
		textField_1.setText("6");
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.fill = GridBagConstraints.BOTH;
		gbc_textField_1.insets = new Insets(0, 0, 5, 0);
		gbc_textField_1.gridx = 2;
		gbc_textField_1.gridy = 0;
		panel1.add(textField_1, gbc_textField_1);
		textField_1.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				textField_1.setText(textField_1.getText().replaceAll("[^0-9]", ""));
			}
		});
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setText("10");
		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
		gbc_textField_2.fill = GridBagConstraints.BOTH;
		gbc_textField_2.insets = new Insets(0, 0, 5, 0);
		gbc_textField_2.gridx = 2;
		gbc_textField_2.gridy = 1;
		panel1.add(textField_2, gbc_textField_2);
		textField_2.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				textField_2.setText(textField_2.getText().replaceAll("[^0-9]", ""));
			}
		});
		textField_2.setColumns(10);



		final JCheckBox chckbxAZ = new JCheckBox("A-Z");
		chckbxAZ.setSelected(true);
		GridBagConstraints gbc_chckbxAZ = new GridBagConstraints();
		gbc_chckbxAZ.fill = GridBagConstraints.BOTH;
		gbc_chckbxAZ.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxAZ.gridx = 0;
		gbc_chckbxAZ.gridy = 1;
		panel1.add(chckbxAZ, gbc_chckbxAZ);


		tabbedPane = new JTabbedPane();
		tabbedPane.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if(tabbedPane.getSelectedIndex()==1){
					ResultSet unprocessedData = conn.executeQuery("SELECT * FROM PASSWORDS");
					DefaultTableModel model = new DefaultTableModel(null, new String[]{"rawPassword","MD5","SHA1"});
					try {
						while(unprocessedData.next()){
							String[] row={unprocessedData.getString("rawPassword"),
									unprocessedData.getString("MD5"),
									unprocessedData.getString("SHA1")};

							model.addRow(row);
						}
					} catch (SQLException e1) {e1.printStackTrace();}
					table.setModel(model);
				}
			}
		});
		tabbedPane.addTab("Generate",panel1);


		final JButton btnGenerateProfit = new JButton("Generate Password");
		GridBagConstraints gbc_btnGenerateProfit = new GridBagConstraints();
		gbc_btnGenerateProfit.gridwidth = 2;
		gbc_btnGenerateProfit.anchor = GridBagConstraints.NORTHWEST;
		gbc_btnGenerateProfit.insets = new Insets(0, 0, 0, 5);
		gbc_btnGenerateProfit.gridx = 1;
		gbc_btnGenerateProfit.gridy = 3;
		panel1.add(btnGenerateProfit, gbc_btnGenerateProfit);
		btnGenerateProfit.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				if( chckbxaz.isSelected()  		   == false &&
						chckbxAZ.isSelected() 		   == false &&
						checkBx09.isSelected() 		   == false &&
						chckbxNewCheckBox.isSelected() == false ){

					JOptionPane.showMessageDialog(frame,"You selected no character groups","Error!",JOptionPane.ERROR_MESSAGE);
					return;
				}

				if(Integer.parseInt(textField_2.getText())<Integer.parseInt(textField_1.getText())){
					JOptionPane.showMessageDialog(frame,"Max Lenght is smaller than Min Lenght","Error!",JOptionPane.ERROR_MESSAGE);
					return;
				}


				boolean devicePresent = refreshRandomBuffer();
				randomText="";
				if(!devicePresent){
					randomList.clear();
					SecureRandom sr = new SecureRandom();
					for(int z=0;z<4096;z++){
						randomList.add(sr.nextInt()%127);
					}
				}

				for(Integer E: randomList){
					if(randomText.length() < Integer.parseInt(textField_2.getText().replaceAll("\\s+", ""))){
						if(chckbxaz.isSelected()){
							if(E.intValue() >= 97 && E.intValue() <= 122){
								randomText+=(char)E.intValue();
							}
						}
						if(chckbxAZ.isSelected()){
							if(E.intValue() >= 65 && E.intValue() <= 90){
								randomText+=(char)E.intValue();
							}
						}
						if(checkBx09.isSelected()){
							if(E.intValue() >= 48 && E.intValue() <= 57){
								randomText+=(char)E.intValue();
							}
						}
						if(chckbxNewCheckBox.isSelected()){
							if((E.intValue() >= 33 && E.intValue()  <= 47) ||
									(E.intValue() >= 60 && E.intValue()  <= 63) ||
									(E.intValue() >= 91 && E.intValue()  <= 95) ||
									(E.intValue() >= 123 && E.intValue() <= 126 )){
								randomText+=(char)E.intValue();
							}
						}
					}
				}
				if(devicePresent){
					if(randomText.length() < Integer.parseInt(textField_1.getText().replaceAll("\\s+", ""))){
						btnGenerateProfit.doClick();
					}
				}
				int n = JOptionPane.showConfirmDialog(frame,
						"Would you like to save your newly\ngenerated password in the database?",
						"Save Password?",
						JOptionPane.YES_NO_OPTION);
				boolean answer = (n==0) ? true : false ; 
				if(answer){ //upload results
					String md5  = toMD5(randomText);
					String sha1 = toSHA1(randomText);
					if(conn==null) return;
					conn.executeUpdate("INSERT INTO \"main\".\"passwords\" (\"rawPassword\",\"MD5\",\"SHA1\") VALUES (?,?,?)",randomText,md5,sha1);
					//conn.commit();
				}


			}
		});




		tabbedPane.addTab("Manage", panel2);
		panel2.setLayout(new BorderLayout());

		ResultSet unprocessedData = conn.executeQuery("SELECT * FROM PASSWORDS");
		DefaultTableModel model = new DefaultTableModel(null, new String[]{"rawPassword","MD5","SHA1"}){
			public void removeRow(int row)
			{
			    fireTableRowsDeleted(row, row);
			}
		};

		try {
			while(unprocessedData.next()){
				String[] row={unprocessedData.getString("rawPassword"),
						unprocessedData.getString("MD5"),
						unprocessedData.getString("SHA1")};

				model.addRow(row);
			}
		} catch (SQLException e1) {e1.printStackTrace();}

		table = new JTable(){
			public boolean isCellEditable(int row,int column){ //disable editing
				return false;
			}
		};
		table.setModel(model);
		table.setFillsViewportHeight(true);
		final JPopupMenu popupMenu = new JPopupMenu();
		
		
		
		
		JMenuItem sha1ClipItem = new JMenuItem("Copy SHA1 to Clipboard");
		sha1ClipItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			    StringSelection selection = new StringSelection((String) table.getModel().getValueAt(table.getSelectedRow(), 2));
			    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			    clipboard.setContents(selection, selection);
			}
		});
		popupMenu.add(sha1ClipItem);
		
		
		JMenuItem md5ClipItem = new JMenuItem("Copy MD5 to Clipboard");
		md5ClipItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			    StringSelection selection = new StringSelection((String) table.getModel().getValueAt(table.getSelectedRow(), 1));
			    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			    clipboard.setContents(selection, selection);
			}
		});
		popupMenu.add(md5ClipItem);
		
		
		
		JMenuItem passwdClipItem = new JMenuItem("Copy Password to Clipboard");
		passwdClipItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			    StringSelection selection = new StringSelection((String) table.getModel().getValueAt(table.getSelectedRow(), 0));
			    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			    clipboard.setContents(selection, selection);
			}
		});
		popupMenu.add(passwdClipItem);
		
		
		JMenuItem deleteItem = new JMenuItem("Delete");
		deleteItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selected = table.getSelectedRow();
				try{
					Object sha1obj = (Object) table.getModel().getValueAt(selected, 2);
					boolean result = conn.executeUpdate("DELETE FROM passwords WHERE SHA1=?", (String) sha1obj);
					if(result){
						JOptionPane.showMessageDialog(frame, "Record deleted succesfully");
						((DefaultTableModel)table.getModel()).removeRow(selected);
					}else{
						JOptionPane.showMessageDialog(frame, "Record not deleted");
					}
				}catch(ArrayIndexOutOfBoundsException lel){
					JOptionPane.showMessageDialog(frame, "Please Select a Row");
				}
			}
		});
		popupMenu.add(deleteItem);

		
		table.setComponentPopupMenu(popupMenu);

		JScrollPane scrollPane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		panel2.add(scrollPane, BorderLayout.CENTER);
		topPanel.add( tabbedPane, BorderLayout.CENTER );

	}


	void saveDatabase(){

		//find the jar's name
		String jarname;
		String path = System.getProperty("java.class.path");
		Pattern p = Pattern.compile("\\w*.jar"); //improve this
		Matcher m = p.matcher(path);
		if(m.find()){
			if(m.group(0).equals(path)){
				jarname=m.group(0);
				if(!new File(m.group(0)).exists()){
					System.err.println("Someting went very very wrong");
					return;
				}
			}else{
				System.err.println("In order to save you must run the program from a jar");
				return;
			}
		}else{
			System.err.println("In order to save you must run the program from a jar");
			return;
		}

		if(!removed){ // if we need to add our jar file to removal
			new File(jarname).deleteOnExit();
		}



		//create a new copy
		File source = new File(jarname);
		File dest;
		if(jarname.charAt(0)=='1'){
			dest = new File(jarname.substring(1, jarname.length()));
		}else{
			dest = new File("1"+jarname);
		}
		if(!dest.exists()){
			try {
				Files.copy(source.toPath(), dest.toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if(!new File("database.db").exists()) extractDatabase();

		Path myFilePath = Paths.get("database.db");
		Path zipFilePath = Paths.get(dest.getAbsolutePath());
		FileSystem fs;
		try {
			fs = FileSystems.newFileSystem(zipFilePath, null);
			Path fileInsideZipPath = fs.getPath("/com/afonsobordado/ORANGE/database.db");
			Files.delete(fileInsideZipPath);
			Files.copy(myFilePath, fileInsideZipPath);
			fs.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//cleanup

		ProcessBuilder pb = new ProcessBuilder("java", "-jar", dest.getName(),jarname);
		try {
			pb.start();
		} catch (IOException e) {
			e.printStackTrace();
		}


	}
	boolean refreshRandomBuffer(){
		boolean returnVal=false;
		randomList.clear();
		copyFromJar();
		if(System.getProperty("os.name").toLowerCase().startsWith("windows")){
			String dev = execCmd("./hidtool.exe write");
			System.out.println(dev);
			if(!dev.equals("") || !dev.equals("\n")) return false;
			String val = execCmd("./hidtool.exe read");
			Pattern p = Pattern.compile("0x([0-9|a-f]{2})\\s*");
			Matcher m = p.matcher(val);
			while(m.find()){
				String match = m.group(1);
				randomList.add(Integer.parseInt(match,16));
			}
			new File("hidtool.exe").delete();
			returnVal=true;
		}else{ //linix
			execCmd("chmod 777 ./hidtool");
			File runSh = new File("run.sh");
			PrintWriter printWriter=null;
			try {
				printWriter = new PrintWriter(runSh);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			printWriter.println("#!/bin/bash");
			printWriter.println("./hidtool write");
			printWriter.println("./hidtool read");
			printWriter.close();

			execCmd("chmod 777 ./run.sh");


			String rand = execCmd("gksudo ./run.sh");

			Pattern p = Pattern.compile("0x([0-9|a-f]{2})\\s*");

			Matcher m = p.matcher(rand);
			int found=0;
			while(m.find()){
				String match = m.group(1);
				randomList.add(Integer.parseInt(match,16));
				found++;
			}
			execCmd("rm ./hidtool ./run.sh");
			if(found==0) return false;
			returnVal=true;
		}
		if(randomList==null){
			refreshRandomBuffer();
		}
		return returnVal;

	}


	String execCmd(String cmd){

		Process p =null;

		try {
			p = Runtime.getRuntime().exec(cmd);
			p.waitFor();
		} catch (IOException e2) {
			e2.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}



		BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line = "";

		String buf="";
		try {


			while ((line = b.readLine())!=null){
				buf+=line;
			}

		} catch (IOException e1) {
			e1.printStackTrace();
		}

		try {
			b.close();
		} catch (IOException e) {
			e.printStackTrace();
		}


		return buf;
	}

	void extractDatabase(){
		InputStream ddlStream = this.getClass().getClassLoader().getResourceAsStream("com/afonsobordado/ORANGE/database.db");
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream("database.db");
			byte[] buf = new byte[2048];
			int r = ddlStream.read(buf);
			while(r != -1) {
				fos.write(buf, 0, r);
				r = ddlStream.read(buf);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		File dataFile = new File("database.db");
		if(dataFile.exists()) dataFile.deleteOnExit(); // add to jvm garbage collection on exit

	}

	void copyFromJar (){
		InputStream ddlStream;
		if(System.getProperty("os.name").toLowerCase().startsWith("windows")){
			ddlStream = com.afonsobordado.ORANGE.MainWindow.class.getClassLoader().getResourceAsStream("com/afonsobordado/ORANGE/hidtool.exe");
		}else{
			ddlStream = com.afonsobordado.ORANGE.MainWindow.class.getClassLoader().getResourceAsStream("com/afonsobordado/ORANGE/hidtool");
		}
		FileOutputStream fos = null;
		try {
			if(System.getProperty("os.name").toLowerCase().startsWith("windows")){
				fos = new FileOutputStream("hidtool.exe");
			}else{
				fos = new FileOutputStream("hidtool");
			}
			byte[] buf = new byte[2048];
			int r = ddlStream.read(buf);
			while(r != -1) {
				fos.write(buf, 0, r);
				r = ddlStream.read(buf);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public String toMD5(String md5) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] array = md.digest(md5.getBytes());
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < array.length; ++i) {
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {System.err.println("Why dont you have MD5? it was invented in 1992");}
		return null;
	}

	public static String toSHA1(String input) {
		MessageDigest mDigest;
		try {
			mDigest = MessageDigest.getInstance("SHA1");
			byte[] result = mDigest.digest(input.getBytes());
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < result.length; i++) {
				sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {System.err.println("Why dont you have Sha1? it was invented in 1995");}
		return null;
	}
}


