package com.afonsobordado.ORANGE;

import java.sql.*;

public class SQLiteJDBC {
	private boolean gotConnection;
	private Connection c = null;
	public SQLiteJDBC(String dbname){
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + dbname);
			c.setAutoCommit(true);
			
		}catch(Exception e){
			gotConnection=false;
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}
		gotConnection=true;
	}
	
	public boolean getConnectionStatus(){
		return gotConnection;
	}
	
	
	public boolean executeUpdate(String query,String ... args){ // also deletes and inserts
		PreparedStatement statement;
		try {
			statement = c.prepareStatement(query);
			int counter=1;
			for(String currArg:args){
				statement.setString(counter, currArg);
				counter++;
			}
	        statement.executeUpdate();
	        return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
	}
	
	public ResultSet executeQuery(String query,String ... args){ // also deletes and inserts
		PreparedStatement statement;
		try {
			statement = c.prepareStatement(query);
			int counter=1;
			for(String currArg:args){
				statement.setString(counter, currArg);
				counter++;
			}
			return statement.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	/*public void commit(){
		try {
			c.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}*/
	
	public void closeConnection(){
		try {
			//c.commit();
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Statement getStatement(String string) {
		try {
			return c.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
