ORANGE
======

Oftenly Random Number Generator

ORANGE is a hardware usb entropy generator using the jitter of the internal watchdog timer of the attiny85
as a seed for a cryptographically safe PRNG.

How It Works?
======

Using V-usb on the atTiny85 we are able to talk through USB with the host.

Based on the timing of the usb write/read messages we seed a cryptographically secure PRNG and save the result to eeprom, on a read request we return X (TBD) bytes of the eeprom.
