#include "matrix.h"

//This variable holds the colours of all the led's on the current iteration
led_colour matrix[20] = { 0 };

void matrix_set_colour(uint8_t face, uint8_t led, led_colour colour) {
	//This is the same as matrix[face][led]
	//but our matrix is only one dimentional so we do it this way
	matrix[(face * 4) + led] = colour;
}

led_colour matrix_get_colour(uint8_t face, uint8_t led) {
	//This is the same as matrix[face][led]
	//but our matrix is only one dimentional so we do it this way
	return matrix[(face * 4) + led];
}

static uint8_t current_led = 0; // specifies the current led line which is being updated 0 - 3
static uint8_t current_output = 1; // specifies the current light cycle (only 1 bit high at a time)

//matrix_next_cycle calculates the next transfer value for the shift register
//transmits it to Shift register and selects the apropriate output transistor
uint8_t matrix_next_cycle() {

	uint16_t transfer = 0;
	for (uint8_t i = 0; i < 5; i++) {
		led_colour colour = matrix_get_colour(i, current_led);
		if (colour.g & current_output)							//if the colour has a on bit for our 
			transfer |= 1 << ((i * 3) + 0);						//current light output we calculate 
		if (colour.b & current_output)							//the shift necessary amount and OR 
			transfer |= 1 << ((i * 3) + 1);						//it to the transfer queue, doing it
		if (colour.r & current_output)							//this way means we can dump an entire
			transfer |= 1 << ((i * 3) + 2);						//row at a time, giving better brightness
	}

	
	PORTB &= ~(0x02);			//Select chip
	SPI_Transmit(transfer);		//Transfer Row data to Shift Register

	PORTD = (1 << current_led); //Enable Row  - These two need to be together
	PORTB |= 0x02;				//Latch chip  - or some of the light might bleed
								//			  - into the next row

	current_output <<= 1; //Next light cycle

	if (current_output == 0 && current_led == 3) { // end of update cycle
		current_output = 1; //prepare next cycle
		current_led = 0;	//prepare next cycle

		return CYCLE_END_MATRIX;
	} else if (current_output == 0) { //end of light cycle
		current_output = 1; //prepare next cycle
		current_led++;

		return CYCLE_END_LINE;
	} else { //still on light cycle
		return CYCLE_ONGOING_LINE;
	}

}

//Does a full matrix cycle
void dump_matrix() {
	while (matrix_next_cycle() != CYCLE_END_MATRIX);
}

//turns all matrix led's off
void paint_matrix(const led_colour z) {
	for (uint8_t i = 0; i < 4; i++)
		for (uint8_t j = 0; j < 5; j++)
			matrix_set_colour(j, i, z);
}