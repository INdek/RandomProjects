#include "CSPI.h"

void SPI_init() {
	// SPI ENABLE CODE

	//Set SPI pins as outputs
	DDRB = 0x3E;

	// SPE � SPI Enable
	// MSTR � Master/Slave Select - Select AVR as SPI Master
	SPCR = ((1 << SPE) | (1 << MSTR));
	//SPI2X - CLOCK RATE SELECT - Fosc(16MHz) / 2 - 8MHz transfer clock
	SPSR = (1 << SPI2X);

}

//Inlining this has performance improvements
//Transmit 16 bits of data trough the SPI Bus
void SPI_Transmit(uint16_t data) {
	SPDR = ((data & 0xff00) >> 8); //Queue high byte for spi transfer
	while (!(SPSR & (1 << SPIF))); //Wait for transfer to be complete (usualy 500nS)
								   //There is no reason to wait for an interrupt
								   //Because this loop takes ~16 clock cycles to complete

	SPDR = (data & 0x00ff);		   //Queue Low Bit
	while (!(SPSR & (1 << SPIF)));
}

