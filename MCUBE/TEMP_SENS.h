#ifndef _TEMP_SENS_h
#define _TEMP_SENS_h
#include <avr/io.h>
#include <avr/interrupt.h>
#ifdef __cplusplus
extern "C" {
#endif
//Simple one point calibration value, no coefficient
//Good enough for a +/- 15 degC range without too much
//error at the ends
#define TEMP_CALIB_VAL 57

	void temp_init();
	int8_t temp_read();

#ifdef __cplusplus
}
#endif
#endif

