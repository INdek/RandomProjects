#include "TEMP_SENS.h"

void temp_init() {
	//128 prescaler (As slow as possible)
	ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);

	//ADC referenced by the internal 1.1v voltage reference
	ADMUX |= (1 << REFS0) | (1 << REFS1);

	//ADC Select ADC8 input (Temperature sensor)
	ADMUX |= (1 << MUX3);

	//Enable ADC
	ADCSRA |= (1 << ADEN);

	//The first sample of the ADC takes 25 ADC cylces to complete
	//the extra 12 ADC Cycles are used to enable output circuitrey
	//this may skew other results
	temp_read();
}
//plot measurements
//15.0c = 70
//20.0c = 74
//30.0c = 82


//real world measurements
//24.5c = 77
//26.0c = 78
//27.0c = 79

int8_t temp_read() {
	//Start ADC conversion
	ADCSRA |= (1 << ADSC);

	//ADC Conversion takes more than 13 ADC clocks to finish
	//Total time on this loop is more than (13*128) = ~1664 CPU cycles
	//Wait for conversion to finish
	while (ADCSRA & (1 << ADSC));

	//Get 10bit value out of ADC
	return ADCW;
}