#ifndef _LED_COLOUR_h
#define _LED_COLOUR_h
#include <avr/io.h>
#ifdef __cplusplus
extern "C" {
#endif

//Amount of each colour to be presented on the led
//The actual value depends on the Hamming weight
// 0b00001111 = 0b01010101 = 50% duty cycle
// Due to the way the eye precieves brighteness
// this linear relationship might not represent
// real world conditions (ie. 50% duty cycle is almost as bright as 100%)
// when chosing which values to display
// a better way might be to square the intended value
// and displaying the 8 MSB's
typedef struct led_colour {
	uint8_t r;
	uint8_t g;
	uint8_t b; 
} led_colour;


#ifdef __cplusplus
}
#endif
#endif // !_LED_COLOUR_h
