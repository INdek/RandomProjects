EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:MCUBE-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L R R28
U 1 1 56B0BC34
P 5950 3600
F 0 "R28" V 6030 3600 50  0000 C CNN
F 1 "1K" V 5950 3600 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Vertical_RM5mm" V 5880 3600 30  0001 C CNN
F 3 "" H 5950 3600 30  0000 C CNN
	1    5950 3600
	0    1    -1   0   
$EndComp
$Comp
L R R27
U 1 1 56B0BE5D
P 5950 3400
F 0 "R27" V 6030 3400 50  0000 C CNN
F 1 "1K" V 5950 3400 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Vertical_RM5mm" V 5880 3400 30  0001 C CNN
F 3 "" H 5950 3400 30  0000 C CNN
	1    5950 3400
	0    1    -1   0   
$EndComp
$Comp
L R R26
U 1 1 56B0BE63
P 5950 3200
F 0 "R26" V 6030 3200 50  0000 C CNN
F 1 "1K" V 5950 3200 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Vertical_RM5mm" V 5880 3200 30  0001 C CNN
F 3 "" H 5950 3200 30  0000 C CNN
	1    5950 3200
	0    1    -1   0   
$EndComp
$Comp
L R R25
U 1 1 56B0BE69
P 5950 3000
F 0 "R25" V 6030 3000 50  0000 C CNN
F 1 "1K" V 5950 3000 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Vertical_RM5mm" V 5880 3000 30  0001 C CNN
F 3 "" H 5950 3000 30  0000 C CNN
	1    5950 3000
	0    1    -1   0   
$EndComp
Text GLabel 5150 750  2    60   Input ~ 0
MOSI
$Comp
L R R15
U 1 1 56ACB07B
P 4200 3250
F 0 "R15" V 4280 3250 50  0000 C CNN
F 1 "68R" V 4200 3250 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Vertical_RM5mm" V 4130 3250 30  0001 C CNN
F 3 "" H 4200 3250 30  0000 C CNN
	1    4200 3250
	-1   0    0    -1  
$EndComp
$Comp
L R R14
U 1 1 56ACDCA0
P 4000 3250
F 0 "R14" V 4080 3250 50  0000 C CNN
F 1 "68R" V 4000 3250 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Vertical_RM5mm" V 3930 3250 30  0001 C CNN
F 3 "" H 4000 3250 30  0000 C CNN
	1    4000 3250
	-1   0    0    -1  
$EndComp
$Comp
L R R13
U 1 1 56ACDCA6
P 3800 3250
F 0 "R13" V 3880 3250 50  0000 C CNN
F 1 "68R" V 3800 3250 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Vertical_RM5mm" V 3730 3250 30  0001 C CNN
F 3 "" H 3800 3250 30  0000 C CNN
	1    3800 3250
	-1   0    0    -1  
$EndComp
$Comp
L R R12
U 1 1 56ACDCAC
P 3500 3250
F 0 "R12" V 3580 3250 50  0000 C CNN
F 1 "68R" V 3500 3250 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Vertical_RM5mm" V 3430 3250 30  0001 C CNN
F 3 "" H 3500 3250 30  0000 C CNN
	1    3500 3250
	-1   0    0    -1  
$EndComp
$Comp
L R R11
U 1 1 56ACDCB2
P 3300 3250
F 0 "R11" V 3380 3250 50  0000 C CNN
F 1 "68R" V 3300 3250 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Vertical_RM5mm" V 3230 3250 30  0001 C CNN
F 3 "" H 3300 3250 30  0000 C CNN
	1    3300 3250
	-1   0    0    -1  
$EndComp
$Comp
L R R10
U 1 1 56ACDCB8
P 3100 3250
F 0 "R10" V 3180 3250 50  0000 C CNN
F 1 "68R" V 3100 3250 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Vertical_RM5mm" V 3030 3250 30  0001 C CNN
F 3 "" H 3100 3250 30  0000 C CNN
	1    3100 3250
	-1   0    0    -1  
$EndComp
$Comp
L R R8
U 1 1 56ACDCC4
P 2600 3250
F 0 "R8" V 2680 3250 50  0000 C CNN
F 1 "68R" V 2600 3250 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Vertical_RM5mm" V 2530 3250 30  0001 C CNN
F 3 "" H 2600 3250 30  0000 C CNN
	1    2600 3250
	-1   0    0    -1  
$EndComp
$Comp
L R R9
U 1 1 56ACDCCA
P 2800 3250
F 0 "R9" V 2880 3250 50  0000 C CNN
F 1 "68R" V 2800 3250 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Vertical_RM5mm" V 2730 3250 30  0001 C CNN
F 3 "" H 2800 3250 30  0000 C CNN
	1    2800 3250
	-1   0    0    -1  
$EndComp
$Comp
L R R7
U 1 1 56ACDCD0
P 2400 3250
F 0 "R7" V 2480 3250 50  0000 C CNN
F 1 "68R" V 2400 3250 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Vertical_RM5mm" V 2330 3250 30  0001 C CNN
F 3 "" H 2400 3250 30  0000 C CNN
	1    2400 3250
	-1   0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 56AD2ADB
P 2100 3250
F 0 "R6" V 2180 3250 50  0000 C CNN
F 1 "68R" V 2100 3250 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Vertical_RM5mm" V 2030 3250 30  0001 C CNN
F 3 "" H 2100 3250 30  0000 C CNN
	1    2100 3250
	-1   0    0    -1  
$EndComp
$Comp
L R R5
U 1 1 56AD2AE1
P 1900 3250
F 0 "R5" V 1980 3250 50  0000 C CNN
F 1 "68R" V 1900 3250 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Vertical_RM5mm" V 1830 3250 30  0001 C CNN
F 3 "" H 1900 3250 30  0000 C CNN
	1    1900 3250
	-1   0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 56AD2AE7
P 1700 3250
F 0 "R4" V 1780 3250 50  0000 C CNN
F 1 "68R" V 1700 3250 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Vertical_RM5mm" V 1630 3250 30  0001 C CNN
F 3 "" H 1700 3250 30  0000 C CNN
	1    1700 3250
	-1   0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 56AD2AED
P 1200 3250
F 0 "R2" V 1280 3250 50  0000 C CNN
F 1 "68R" V 1200 3250 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Vertical_RM5mm" V 1130 3250 30  0001 C CNN
F 3 "" H 1200 3250 30  0000 C CNN
	1    1200 3250
	-1   0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 56AD2AF3
P 1400 3250
F 0 "R3" V 1480 3250 50  0000 C CNN
F 1 "68R" V 1400 3250 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Vertical_RM5mm" V 1330 3250 30  0001 C CNN
F 3 "" H 1400 3250 30  0000 C CNN
	1    1400 3250
	-1   0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 56AD2AF9
P 1000 3250
F 0 "R1" V 1080 3250 50  0000 C CNN
F 1 "68R" V 1000 3250 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Vertical_RM5mm" V 930 3250 30  0001 C CNN
F 3 "" H 1000 3250 30  0000 C CNN
	1    1000 3250
	-1   0    0    -1  
$EndComp
$Comp
L LED_RCBG D30
U 1 1 56B83D54
P 4000 3750
F 0 "D30" V 3800 3850 50  0000 C CNN
F 1 "LED_RCBG" V 3800 3550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 4000 3700 50  0001 C CNN
F 3 "" H 4000 3700 50  0000 C CNN
	1    4000 3750
	0    1    -1   0   
$EndComp
$Comp
L LED_RCBG D24
U 1 1 56B8433E
P 3300 3750
F 0 "D24" V 3100 3850 50  0000 C CNN
F 1 "LED_RCBG" V 3100 3550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 3300 3700 50  0001 C CNN
F 3 "" H 3300 3700 50  0000 C CNN
	1    3300 3750
	0    1    -1   0   
$EndComp
$Comp
L LED_RCBG D18
U 1 1 56B84344
P 2600 3750
F 0 "D18" V 2400 3850 50  0000 C CNN
F 1 "LED_RCBG" V 2400 3550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 2600 3700 50  0001 C CNN
F 3 "" H 2600 3700 50  0000 C CNN
	1    2600 3750
	0    1    -1   0   
$EndComp
$Comp
L LED_RCBG D12
U 1 1 56B8434A
P 1900 3750
F 0 "D12" V 1700 3850 50  0000 C CNN
F 1 "LED_RCBG" V 1700 3550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 1900 3700 50  0001 C CNN
F 3 "" H 1900 3700 50  0000 C CNN
	1    1900 3750
	0    1    -1   0   
$EndComp
$Comp
L LED_RCBG D6
U 1 1 56B84350
P 1200 3750
F 0 "D6" V 1000 3850 50  0000 C CNN
F 1 "LED_RCBG" V 1000 3550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 1200 3700 50  0001 C CNN
F 3 "" H 1200 3700 50  0000 C CNN
	1    1200 3750
	0    1    -1   0   
$EndComp
$Comp
L LED_RCBG D29
U 1 1 56B84CE0
P 3950 4450
F 0 "D29" V 3750 4550 50  0000 C CNN
F 1 "LED_RCBG" V 3750 4250 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 3950 4400 50  0001 C CNN
F 3 "" H 3950 4400 50  0000 C CNN
	1    3950 4450
	0    1    -1   0   
$EndComp
$Comp
L LED_RCBG D23
U 1 1 56B84CE6
P 3250 4450
F 0 "D23" V 3050 4550 50  0000 C CNN
F 1 "LED_RCBG" V 3050 4250 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 3250 4400 50  0001 C CNN
F 3 "" H 3250 4400 50  0000 C CNN
	1    3250 4450
	0    1    -1   0   
$EndComp
$Comp
L LED_RCBG D17
U 1 1 56B84CEC
P 2550 4450
F 0 "D17" V 2350 4550 50  0000 C CNN
F 1 "LED_RCBG" V 2350 4250 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 2550 4400 50  0001 C CNN
F 3 "" H 2550 4400 50  0000 C CNN
	1    2550 4450
	0    1    -1   0   
$EndComp
$Comp
L LED_RCBG D11
U 1 1 56B84CF2
P 1850 4450
F 0 "D11" V 1650 4550 50  0000 C CNN
F 1 "LED_RCBG" V 1650 4250 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 1850 4400 50  0001 C CNN
F 3 "" H 1850 4400 50  0000 C CNN
	1    1850 4450
	0    1    -1   0   
$EndComp
$Comp
L LED_RCBG D5
U 1 1 56B84CF8
P 1150 4450
F 0 "D5" V 950 4550 50  0000 C CNN
F 1 "LED_RCBG" V 950 4250 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 1150 4400 50  0001 C CNN
F 3 "" H 1150 4400 50  0000 C CNN
	1    1150 4450
	0    1    -1   0   
$EndComp
$Comp
L LED_RCBG D28
U 1 1 56B85B30
P 3900 5150
F 0 "D28" V 3700 5250 50  0000 C CNN
F 1 "LED_RCBG" V 3700 4950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 3900 5100 50  0001 C CNN
F 3 "" H 3900 5100 50  0000 C CNN
	1    3900 5150
	0    1    -1   0   
$EndComp
$Comp
L LED_RCBG D22
U 1 1 56B85B36
P 3200 5150
F 0 "D22" V 3000 5250 50  0000 C CNN
F 1 "LED_RCBG" V 3000 4950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 3200 5100 50  0001 C CNN
F 3 "" H 3200 5100 50  0000 C CNN
	1    3200 5150
	0    1    -1   0   
$EndComp
$Comp
L LED_RCBG D16
U 1 1 56B85B3C
P 2500 5150
F 0 "D16" V 2300 5250 50  0000 C CNN
F 1 "LED_RCBG" V 2300 4950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 2500 5100 50  0001 C CNN
F 3 "" H 2500 5100 50  0000 C CNN
	1    2500 5150
	0    1    -1   0   
$EndComp
$Comp
L LED_RCBG D10
U 1 1 56B85B42
P 1800 5150
F 0 "D10" V 1600 5250 50  0000 C CNN
F 1 "LED_RCBG" V 1600 4950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 1800 5100 50  0001 C CNN
F 3 "" H 1800 5100 50  0000 C CNN
	1    1800 5150
	0    1    -1   0   
$EndComp
$Comp
L LED_RCBG D4
U 1 1 56B85B48
P 1100 5150
F 0 "D4" V 900 5250 50  0000 C CNN
F 1 "LED_RCBG" V 900 4950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 1100 5100 50  0001 C CNN
F 3 "" H 1100 5100 50  0000 C CNN
	1    1100 5150
	0    1    -1   0   
$EndComp
$Comp
L LED_RCBG D21
U 1 1 56B85B66
P 3150 5850
F 0 "D21" V 2950 5950 50  0000 C CNN
F 1 "LED_RCBG" V 2950 5650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 3150 5800 50  0001 C CNN
F 3 "" H 3150 5800 50  0000 C CNN
	1    3150 5850
	0    1    -1   0   
$EndComp
$Comp
L LED_RCBG D15
U 1 1 56B85B6C
P 2450 5850
F 0 "D15" V 2250 5950 50  0000 C CNN
F 1 "LED_RCBG" V 2250 5650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 2450 5800 50  0001 C CNN
F 3 "" H 2450 5800 50  0000 C CNN
	1    2450 5850
	0    1    -1   0   
$EndComp
$Comp
L LED_RCBG D9
U 1 1 56B85B72
P 1750 5850
F 0 "D9" V 1550 5950 50  0000 C CNN
F 1 "LED_RCBG" V 1550 5650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 1750 5800 50  0001 C CNN
F 3 "" H 1750 5800 50  0000 C CNN
	1    1750 5850
	0    1    -1   0   
$EndComp
$Comp
L LED_RCBG D3
U 1 1 56B85B78
P 1050 5850
F 0 "D3" V 850 5950 50  0000 C CNN
F 1 "LED_RCBG" V 850 5650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 1050 5800 50  0001 C CNN
F 3 "" H 1050 5800 50  0000 C CNN
	1    1050 5850
	0    1    -1   0   
$EndComp
Text GLabel 4650 550  2    60   Input ~ 0
AE
$Comp
L LED_RCBG D27
U 1 1 56B85B60
P 3850 5850
F 0 "D27" V 3650 5950 50  0000 C CNN
F 1 "LED_RCBG" V 3650 5650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 3850 5800 50  0001 C CNN
F 3 "" H 3850 5800 50  0000 C CNN
	1    3850 5850
	0    1    -1   0   
$EndComp
$Comp
L ATMEGA328P-P IC1
U 1 1 56B35248
P 9050 2900
F 0 "IC1" H 8300 4150 40  0000 L BNN
F 1 "ATMEGA328P-P" H 9450 1500 40  0000 L BNN
F 2 "Housings_DIP:DIP-28_W7.62mm" H 8700 2950 30  0000 C CIN
F 3 "" H 9050 2900 60  0000 C CNN
	1    9050 2900
	-1   0    0    -1  
$EndComp
$Comp
L Crystal Y0
U 1 1 56B3C374
P 7350 1500
F 0 "Y0" H 7350 1650 50  0000 C CNN
F 1 "16MHz" H 7350 1350 50  0000 C CNN
F 2 "Custom_Crystals:Crystal_HC49-U_Vertical_RevA_09Aug2010" H 7350 1500 60  0001 C CNN
F 3 "" H 7350 1500 60  0000 C CNN
	1    7350 1500
	-1   0    0    1   
$EndComp
$Comp
L C C1
U 1 1 56B3C7FD
P 7500 1150
F 0 "C1" H 7525 1250 50  0000 L CNN
F 1 "22pF" H 7525 1050 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 7538 1000 30  0001 C CNN
F 3 "" H 7500 1150 60  0000 C CNN
	1    7500 1150
	-1   0    0    1   
$EndComp
$Comp
L C C0
U 1 1 56B3C8A4
P 7200 1150
F 0 "C0" H 7225 1250 50  0000 L CNN
F 1 "22pF" H 7225 1050 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 7238 1000 30  0001 C CNN
F 3 "" H 7200 1150 60  0000 C CNN
	1    7200 1150
	-1   0    0    1   
$EndComp
Text GLabel 7800 1900 0    39   Input ~ 0
AE
Text GLabel 7800 2200 0    39   Input ~ 0
SCLK
Text GLabel 7800 2100 0    39   Input ~ 0
MISO
Text GLabel 7800 2000 0    39   Input ~ 0
MOSI
$Comp
L AVR-ISP-6 CON1
U 1 1 56B7B89E
P 10350 4900
F 0 "CON1" H 10245 5140 50  0000 C CNN
F 1 "AVR-ISP-6" H 10085 4670 50  0000 L BNN
F 2 "Socket_Strips:Socket_Strip_Straight_2x03" V 9830 4940 50  0001 C CNN
F 3 "" H 10325 4900 60  0000 C CNN
	1    10350 4900
	1    0    0    -1  
$EndComp
Text GLabel 10800 4800 2    39   Input ~ 0
5V
Text GLabel 10800 5000 2    39   Input ~ 0
GND
Text GLabel 10800 4900 2    39   Input ~ 0
MOSI
Text GLabel 9850 4800 0    39   Input ~ 0
MISO
Text GLabel 9850 4900 0    39   Input ~ 0
SCLK
Text GLabel 9850 5000 0    39   Input ~ 0
RST
Text GLabel 7400 3150 0    39   Input ~ 0
RST
Text GLabel 10250 1200 0    39   Input ~ 0
5V
Text GLabel 10650 1200 2    39   Input ~ 0
GND
Text GLabel 2700 950  1    39   Input ~ 0
5V
Text GLabel 3150 950  2    39   Input ~ 0
GND
Text GLabel 4600 950  2    39   Input ~ 0
GND
Text GLabel 4150 950  1    39   Input ~ 0
5V
Text GLabel 4850 650  2    60   Input ~ 0
SCLK
Text GLabel 6200 6050 3    39   Input ~ 0
GND
Text GLabel 7350 850  1    39   Input ~ 0
GND
NoConn ~ 8050 1800
NoConn ~ 8050 2000
NoConn ~ 8050 3800
NoConn ~ 8050 3900
NoConn ~ 8050 4000
NoConn ~ 8050 4100
NoConn ~ 4750 2400
NoConn ~ 4550 2400
$Comp
L USB_OTG P1
U 1 1 56B7BEA6
P 10450 700
F 0 "P1" H 10775 575 50  0000 C CNN
F 1 "USB_OTG" H 10450 900 50  0000 C CNN
F 2 "USB_Connectors:USBMiniTypeB90TroughHole" V 10400 600 60  0001 C CNN
F 3 "" V 10400 600 60  0000 C CNN
	1    10450 700 
	1    0    0    -1  
$EndComp
NoConn ~ 10350 1000
NoConn ~ 10450 1000
NoConn ~ 10550 1000
$Comp
L Q_NPN_EBC Q0
U 1 1 56B7FFAC
P 5350 3600
F 0 "Q0" V 5450 3650 50  0000 R CNN
F 1 "2N3904BU" V 5550 3800 50  0000 R CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Narrow_Oval" H 5550 3700 29  0001 C CNN
F 3 "" H 5350 3600 60  0000 C CNN
	1    5350 3600
	0    -1   1    0   
$EndComp
$Comp
L Q_NPN_EBC Q1
U 1 1 56B80C14
P 5500 4050
F 0 "Q1" V 5600 4100 50  0000 R CNN
F 1 "2N3904BU" V 5700 4250 50  0000 R CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Narrow_Oval" H 5700 4150 29  0001 C CNN
F 3 "" H 5500 4050 60  0000 C CNN
	1    5500 4050
	0    -1   1    0   
$EndComp
$Comp
L Q_NPN_EBC Q2
U 1 1 56B80CCC
P 5650 4500
F 0 "Q2" V 5750 4550 50  0000 R CNN
F 1 "2N3904BU" V 5850 4700 50  0000 R CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Narrow_Oval" H 5850 4600 29  0001 C CNN
F 3 "" H 5650 4500 60  0000 C CNN
	1    5650 4500
	0    -1   1    0   
$EndComp
$Comp
L Q_NPN_EBC Q3
U 1 1 56B810C1
P 5800 4950
F 0 "Q3" V 5900 5000 50  0000 R CNN
F 1 "2N3904BU" V 6000 5150 50  0000 R CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Narrow_Oval" H 6000 5050 29  0001 C CNN
F 3 "" H 5800 4950 60  0000 C CNN
	1    5800 4950
	0    -1   1    0   
$EndComp
$Comp
L C C6
U 1 1 56C512A6
P 6950 1450
F 0 "C6" H 6975 1550 50  0000 L CNN
F 1 "22pF" H 6975 1350 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 6988 1300 30  0001 C CNN
F 3 "" H 6950 1450 60  0000 C CNN
	1    6950 1450
	-1   0    0    -1  
$EndComp
$Comp
L R R20
U 1 1 56C51363
P 6250 1700
F 0 "R20" V 6330 1700 50  0000 C CNN
F 1 "20MR" V 6250 1700 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Vertical_RM5mm" V 6180 1700 30  0001 C CNN
F 3 "" H 6250 1700 30  0000 C CNN
	1    6250 1700
	0    -1   1    0   
$EndComp
Text GLabel 6950 1250 1    39   Input ~ 0
GND
$Comp
L C C5
U 1 1 56C524D5
P 6750 1450
F 0 "C5" H 6775 1550 50  0000 L CNN
F 1 "22pF" H 6775 1350 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 6788 1300 30  0001 C CNN
F 3 "" H 6750 1450 60  0000 C CNN
	1    6750 1450
	-1   0    0    -1  
$EndComp
$Comp
L R R19
U 1 1 56C524DB
P 6050 1900
F 0 "R19" V 6130 1900 50  0000 C CNN
F 1 "20MR" V 6050 1900 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Vertical_RM5mm" V 5980 1900 30  0001 C CNN
F 3 "" H 6050 1900 30  0000 C CNN
	1    6050 1900
	0    -1   1    0   
$EndComp
Text GLabel 6750 1250 1    39   Input ~ 0
GND
$Comp
L C C4
U 1 1 56C525D1
P 6550 1450
F 0 "C4" H 6575 1550 50  0000 L CNN
F 1 "22pF" H 6575 1350 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 6588 1300 30  0001 C CNN
F 3 "" H 6550 1450 60  0000 C CNN
	1    6550 1450
	-1   0    0    -1  
$EndComp
$Comp
L R R18
U 1 1 56C525D7
P 5850 2100
F 0 "R18" V 5930 2100 50  0000 C CNN
F 1 "20MR" V 5850 2100 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Vertical_RM5mm" V 5780 2100 30  0001 C CNN
F 3 "" H 5850 2100 30  0000 C CNN
	1    5850 2100
	0    -1   1    0   
$EndComp
Text GLabel 6550 1250 1    39   Input ~ 0
GND
$Comp
L C C3
U 1 1 56C525E1
P 6350 1450
F 0 "C3" H 6375 1550 50  0000 L CNN
F 1 "22pF" H 6375 1350 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 6388 1300 30  0001 C CNN
F 3 "" H 6350 1450 60  0000 C CNN
	1    6350 1450
	-1   0    0    -1  
$EndComp
$Comp
L R R17
U 1 1 56C525E7
P 5650 2300
F 0 "R17" V 5730 2300 50  0000 C CNN
F 1 "20MR" V 5650 2300 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Vertical_RM5mm" V 5580 2300 30  0001 C CNN
F 3 "" H 5650 2300 30  0000 C CNN
	1    5650 2300
	0    -1   1    0   
$EndComp
Text GLabel 6350 1250 1    39   Input ~ 0
GND
$Comp
L C C2
U 1 1 56C528E7
P 6150 1450
F 0 "C2" H 6175 1550 50  0000 L CNN
F 1 "22pF" H 6175 1350 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 6188 1300 30  0001 C CNN
F 3 "" H 6150 1450 60  0000 C CNN
	1    6150 1450
	-1   0    0    -1  
$EndComp
$Comp
L R R16
U 1 1 56C528ED
P 5450 2500
F 0 "R16" V 5530 2500 50  0000 C CNN
F 1 "20MR" V 5450 2500 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Vertical_RM5mm" V 5380 2500 30  0001 C CNN
F 3 "" H 5450 2500 30  0000 C CNN
	1    5450 2500
	0    -1   1    0   
$EndComp
Text GLabel 6150 1250 1    39   Input ~ 0
GND
Wire Wire Line
	5500 3200 5500 3850
Wire Wire Line
	5350 3000 5350 3400
Wire Wire Line
	3850 1000 3600 1000
Wire Wire Line
	3600 1000 3600 2500
Wire Wire Line
	3600 2500 3300 2500
Wire Wire Line
	4050 1000 4050 650 
Connection ~ 4050 650 
Wire Wire Line
	4600 950  4450 950 
Wire Wire Line
	4450 950  4450 1000
Wire Wire Line
	4150 1000 4150 950 
Wire Wire Line
	4350 1000 4350 550 
Connection ~ 4350 550 
Wire Wire Line
	3300 2500 3300 2400
Wire Wire Line
	1000 3400 1000 3450
Wire Wire Line
	1000 3450 950  3450
Wire Wire Line
	950  3450 950  4150
Wire Wire Line
	950  4150 900  4150
Wire Wire Line
	900  4150 900  4850
Wire Wire Line
	900  4850 850  4850
Wire Wire Line
	850  4850 850  5550
Wire Wire Line
	1200 3450 1150 3450
Wire Wire Line
	1150 3450 1150 4150
Wire Wire Line
	1100 4150 1100 4850
Wire Wire Line
	1150 4150 1100 4150
Wire Wire Line
	1100 4850 1050 4850
Wire Wire Line
	1050 4850 1050 5550
Wire Wire Line
	1200 3400 1200 3450
Wire Wire Line
	1400 3400 1400 3450
Wire Wire Line
	1400 3450 1350 3450
Wire Wire Line
	1350 3450 1350 4150
Wire Wire Line
	1350 4150 1300 4150
Wire Wire Line
	1300 4150 1300 4850
Wire Wire Line
	1300 4850 1250 4850
Wire Wire Line
	1250 4850 1250 5550
Wire Wire Line
	1700 3400 1700 3450
Wire Wire Line
	1900 3400 1900 3450
Wire Wire Line
	2100 3400 2100 3450
Wire Wire Line
	2400 3400 2400 3450
Wire Wire Line
	2600 3400 2600 3450
Wire Wire Line
	2800 3400 2800 3450
Wire Wire Line
	3100 3400 3100 3450
Wire Wire Line
	3300 3400 3300 3450
Wire Wire Line
	3500 3400 3500 3450
Wire Wire Line
	3800 3400 3800 3450
Wire Wire Line
	4000 3400 4000 3450
Wire Wire Line
	1700 3450 1650 3450
Wire Wire Line
	1650 3450 1650 4150
Wire Wire Line
	1900 3450 1850 3450
Wire Wire Line
	1850 3450 1850 4150
Wire Wire Line
	2100 3450 2050 3450
Wire Wire Line
	2050 3450 2050 4150
Wire Wire Line
	2400 3450 2350 3450
Wire Wire Line
	2350 3450 2350 4150
Wire Wire Line
	2600 3450 2550 3450
Wire Wire Line
	2550 3450 2550 4150
Wire Wire Line
	2800 3450 2750 3450
Wire Wire Line
	2750 3450 2750 4150
Wire Wire Line
	3100 3450 3050 3450
Wire Wire Line
	3050 3450 3050 4150
Wire Wire Line
	3300 3450 3250 3450
Wire Wire Line
	3250 3450 3250 4150
Wire Wire Line
	3500 3450 3450 3450
Wire Wire Line
	3450 3450 3450 4150
Wire Wire Line
	3800 3450 3750 3450
Wire Wire Line
	3750 3450 3750 4150
Wire Wire Line
	4000 3450 3950 3450
Wire Wire Line
	3950 3450 3950 4150
Wire Wire Line
	4200 3400 4200 3450
Wire Wire Line
	4200 3450 4150 3450
Wire Wire Line
	4150 3450 4150 4150
Wire Wire Line
	1650 4150 1600 4150
Wire Wire Line
	1600 4150 1600 4850
Wire Wire Line
	1850 4150 1800 4150
Wire Wire Line
	1800 4150 1800 4850
Wire Wire Line
	2050 4150 2000 4150
Wire Wire Line
	2000 4150 2000 4850
Wire Wire Line
	2350 4150 2300 4150
Wire Wire Line
	2300 4150 2300 4850
Wire Wire Line
	2550 4150 2500 4150
Wire Wire Line
	2500 4150 2500 4850
Wire Wire Line
	2750 4150 2700 4150
Wire Wire Line
	2700 4150 2700 4850
Wire Wire Line
	1600 4850 1550 4850
Wire Wire Line
	1550 4850 1550 5550
Wire Wire Line
	1800 4850 1750 4850
Wire Wire Line
	1750 4850 1750 5550
Wire Wire Line
	2000 4850 1950 4850
Wire Wire Line
	1950 4850 1950 5550
Wire Wire Line
	2300 4850 2250 4850
Wire Wire Line
	2250 4850 2250 5550
Wire Wire Line
	2450 5550 2450 4850
Wire Wire Line
	2450 4850 2500 4850
Wire Wire Line
	2700 4850 2650 4850
Wire Wire Line
	2650 4850 2650 5550
Wire Wire Line
	3050 4150 3000 4150
Wire Wire Line
	3000 4150 3000 4850
Wire Wire Line
	3250 4150 3200 4150
Wire Wire Line
	3200 4150 3200 4850
Wire Wire Line
	3450 4150 3400 4150
Wire Wire Line
	3400 4150 3400 4850
Wire Wire Line
	3750 4150 3700 4150
Wire Wire Line
	3700 4150 3700 4850
Wire Wire Line
	3950 4150 3900 4150
Wire Wire Line
	3900 4150 3900 4850
Wire Wire Line
	4150 4150 4100 4150
Wire Wire Line
	4100 4150 4100 4850
Wire Wire Line
	3000 4850 2950 4850
Wire Wire Line
	2950 4850 2950 5550
Wire Wire Line
	3200 4850 3150 4850
Wire Wire Line
	3150 4850 3150 5550
Wire Wire Line
	3350 5550 3350 4850
Wire Wire Line
	3350 4850 3400 4850
Wire Wire Line
	3650 5550 3650 4850
Wire Wire Line
	3650 4850 3700 4850
Wire Wire Line
	3850 5550 3850 4850
Wire Wire Line
	3850 4850 3900 4850
Wire Wire Line
	4050 5550 4050 4850
Wire Wire Line
	4050 4850 4100 4850
Wire Wire Line
	2400 2400 1000 2400
Wire Wire Line
	1000 2400 1000 3100
Wire Wire Line
	1200 3100 1200 2450
Wire Wire Line
	1200 2450 2500 2450
Wire Wire Line
	2500 2450 2500 2400
Wire Wire Line
	1400 3100 1400 2500
Wire Wire Line
	1400 2500 2600 2500
Wire Wire Line
	2600 2500 2600 2400
Wire Wire Line
	1700 3100 1700 2550
Wire Wire Line
	1700 2550 2700 2550
Wire Wire Line
	2700 2550 2700 2400
Wire Wire Line
	1900 3100 1900 2600
Wire Wire Line
	1900 2600 2800 2600
Wire Wire Line
	2800 2600 2800 2400
Wire Wire Line
	2100 3100 2100 2650
Wire Wire Line
	2100 2650 2900 2650
Wire Wire Line
	2900 2650 2900 2400
Wire Wire Line
	2400 3100 2400 2700
Wire Wire Line
	2400 2700 3000 2700
Wire Wire Line
	3000 2700 3000 2400
Wire Wire Line
	2600 3100 2600 2750
Wire Wire Line
	2600 2750 3100 2750
Wire Wire Line
	3100 2750 3100 2400
Wire Wire Line
	2800 3100 2800 2800
Wire Wire Line
	2800 2800 3150 2800
Wire Wire Line
	3150 2800 3150 2550
Wire Wire Line
	3150 2550 3850 2550
Wire Wire Line
	3850 2550 3850 2400
Wire Wire Line
	3100 3100 3100 2850
Wire Wire Line
	3100 2850 3200 2850
Wire Wire Line
	3200 2850 3200 2600
Wire Wire Line
	3200 2600 3950 2600
Wire Wire Line
	3950 2600 3950 2400
Wire Wire Line
	3300 3100 3300 2650
Wire Wire Line
	3300 2650 4050 2650
Wire Wire Line
	4050 2650 4050 2400
Wire Wire Line
	3500 3100 3500 2700
Wire Wire Line
	3500 2700 4150 2700
Wire Wire Line
	4150 2700 4150 2400
Wire Wire Line
	3800 3100 3800 2750
Wire Wire Line
	3800 2750 4250 2750
Wire Wire Line
	4250 2750 4250 2400
Wire Wire Line
	4000 3100 4000 2800
Wire Wire Line
	4000 2800 4350 2800
Wire Wire Line
	4350 2800 4350 2400
Wire Wire Line
	4200 3100 4200 2850
Wire Wire Line
	4200 2850 4450 2850
Wire Wire Line
	4450 2850 4450 2400
Wire Wire Line
	4500 4150 5300 4150
Wire Wire Line
	4550 4600 5450 4600
Wire Wire Line
	4600 5050 5600 5050
Wire Wire Line
	2400 750  2400 1000
Wire Wire Line
	2600 650  2600 1000
Wire Wire Line
	2700 1000 2700 950 
Wire Wire Line
	3000 950  3000 1000
Wire Wire Line
	3150 950  3000 950 
Wire Wire Line
	2900 550  2900 1000
Wire Wire Line
	1200 4050 4450 4050
Wire Wire Line
	4450 4050 4450 3700
Wire Wire Line
	4450 3700 5150 3700
Wire Wire Line
	1150 4750 4500 4750
Wire Wire Line
	4500 4750 4500 4150
Wire Wire Line
	1100 5450 4550 5450
Wire Wire Line
	4550 5450 4550 4600
Wire Wire Line
	1050 6150 4600 6150
Wire Wire Line
	4600 6150 4600 5050
Wire Wire Line
	10300 2100 9950 2100
Connection ~ 10300 1800
Wire Wire Line
	10300 2400 9950 2400
Connection ~ 10300 2100
Wire Wire Line
	10600 4100 9950 4100
Wire Wire Line
	10300 4100 10300 4000
Wire Wire Line
	10300 4000 9950 4000
Connection ~ 10300 4100
Wire Wire Line
	7400 1750 7400 2400
Wire Wire Line
	7400 1750 7500 1750
Wire Wire Line
	7500 1750 7500 1300
Wire Wire Line
	7200 1300 7200 1750
Wire Wire Line
	7200 1750 7300 1750
Wire Wire Line
	7300 1750 7300 2500
Connection ~ 7500 1500
Connection ~ 7200 1500
Wire Wire Line
	7200 950  7200 1000
Wire Wire Line
	7500 950  7200 950 
Wire Wire Line
	7500 950  7500 1000
Wire Wire Line
	7350 950  7350 850 
Connection ~ 7350 950 
Wire Wire Line
	6950 3400 8050 3400
Wire Wire Line
	6900 3500 8050 3500
Wire Wire Line
	6850 3600 8050 3600
Wire Wire Line
	6800 3700 8050 3700
Wire Wire Line
	7800 1900 8050 1900
Wire Wire Line
	7800 2000 7950 2000
Wire Wire Line
	7950 2000 7950 2100
Wire Wire Line
	7950 2100 8050 2100
Wire Wire Line
	8050 2200 7900 2200
Wire Wire Line
	7900 2200 7900 2100
Wire Wire Line
	7900 2100 7800 2100
Wire Wire Line
	7800 2200 7850 2200
Wire Wire Line
	7850 2200 7850 2300
Wire Wire Line
	7850 2300 8050 2300
Wire Wire Line
	7800 3250 8050 3250
Wire Wire Line
	7800 3150 7800 3250
Wire Wire Line
	10200 4800 9850 4800
Wire Wire Line
	9850 4900 10200 4900
Wire Wire Line
	9850 5000 10200 5000
Wire Wire Line
	10450 4800 10800 4800
Wire Wire Line
	10450 4900 10800 4900
Wire Wire Line
	10450 5000 10800 5000
Wire Wire Line
	10650 1200 10600 1200
Connection ~ 10600 1200
Wire Wire Line
	10250 1200 10300 1200
Connection ~ 10300 1200
Wire Wire Line
	5800 3600 5800 4750
Wire Wire Line
	6800 3700 6800 3600
Wire Wire Line
	6800 3600 6100 3600
Wire Wire Line
	5800 3400 5650 3400
Wire Wire Line
	5650 3400 5650 4300
Wire Wire Line
	5500 3200 5800 3200
Wire Wire Line
	5350 3000 5800 3000
Wire Wire Line
	6100 3400 6850 3400
Wire Wire Line
	6850 3400 6850 3600
Wire Wire Line
	6900 3500 6900 3200
Wire Wire Line
	6900 3200 6100 3200
Wire Wire Line
	6950 3000 6950 3400
Wire Wire Line
	6950 3000 6100 3000
Wire Wire Line
	7400 3150 7800 3150
Wire Wire Line
	6200 3700 6200 6050
Wire Wire Line
	5550 3700 6200 3700
Wire Wire Line
	5700 4150 6200 4150
Connection ~ 6200 4150
Wire Wire Line
	5850 4600 6200 4600
Connection ~ 6200 4600
Wire Wire Line
	6000 5050 6200 5050
Connection ~ 6200 5050
Wire Wire Line
	2900 550  4650 550 
Wire Wire Line
	2600 650  4850 650 
Wire Wire Line
	2400 750  5150 750 
Connection ~ 1900 4050
Connection ~ 2600 4050
Connection ~ 3300 4050
Connection ~ 4000 4050
Connection ~ 3950 4750
Connection ~ 3250 4750
Connection ~ 2550 4750
Connection ~ 1850 4750
Connection ~ 1800 5450
Connection ~ 2500 5450
Connection ~ 3200 5450
Connection ~ 3900 5450
Connection ~ 3850 6150
Connection ~ 3150 6150
Connection ~ 2450 6150
Connection ~ 1750 6150
Wire Wire Line
	10300 1800 9950 1800
Wire Wire Line
	10250 1000 10250 1100
Wire Wire Line
	10250 1100 10300 1100
Wire Wire Line
	10300 1100 10300 2400
Wire Wire Line
	10600 1100 10600 4100
Wire Wire Line
	10600 1100 10850 1100
Connection ~ 10650 1100
Wire Wire Line
	10850 1100 10850 600 
Wire Wire Line
	10650 1000 10650 1100
Wire Wire Line
	7400 2400 8050 2400
Wire Wire Line
	7300 2500 8050 2500
Wire Wire Line
	6950 1300 6950 1250
Wire Wire Line
	5950 1700 6100 1700
Wire Wire Line
	6750 1300 6750 1250
Wire Wire Line
	5750 1900 5900 1900
Wire Wire Line
	6550 1300 6550 1250
Wire Wire Line
	5550 2100 5700 2100
Wire Wire Line
	6350 1300 6350 1250
Wire Wire Line
	5350 2300 5500 2300
Wire Wire Line
	6150 1300 6150 1250
Wire Wire Line
	5100 2500 5300 2500
Wire Wire Line
	5100 2500 5100 1050
Wire Wire Line
	5100 1050 7050 1050
Wire Wire Line
	5950 1700 5950 1050
Connection ~ 5950 1050
Wire Wire Line
	5750 1900 5750 1050
Connection ~ 5750 1050
Wire Wire Line
	5350 2300 5350 1050
Connection ~ 5350 1050
Wire Wire Line
	7050 1050 7050 2550
Wire Wire Line
	7050 2550 8000 2550
Wire Wire Line
	8000 2550 8000 2650
Wire Wire Line
	8000 2650 8050 2650
Wire Wire Line
	8050 2750 7950 2750
Wire Wire Line
	7950 2750 7950 2600
Wire Wire Line
	7950 2600 7000 2600
Wire Wire Line
	7000 2600 7000 1700
Wire Wire Line
	7000 1700 6400 1700
Wire Wire Line
	6200 1900 6950 1900
Wire Wire Line
	6950 1900 6950 2650
Wire Wire Line
	6950 2650 7900 2650
Wire Wire Line
	7900 2650 7900 2850
Wire Wire Line
	7900 2850 8050 2850
Wire Wire Line
	8050 2950 7850 2950
Wire Wire Line
	7850 2950 7850 2700
Wire Wire Line
	7850 2700 6900 2700
Wire Wire Line
	6900 2700 6900 2100
Wire Wire Line
	6900 2100 6000 2100
Wire Wire Line
	5800 2300 6850 2300
Wire Wire Line
	6850 2300 6850 2750
Wire Wire Line
	6850 2750 7800 2750
Wire Wire Line
	7800 2750 7800 3000
Wire Wire Line
	7800 3000 8000 3000
Wire Wire Line
	8000 3000 8000 3050
Wire Wire Line
	8000 3050 8050 3050
Wire Wire Line
	5600 2500 6800 2500
Wire Wire Line
	6800 2500 6800 2800
Wire Wire Line
	6800 2800 7750 2800
Wire Wire Line
	7750 2800 7750 3050
Wire Wire Line
	7750 3050 7950 3050
Wire Wire Line
	7950 3050 7950 3150
Wire Wire Line
	7950 3150 8050 3150
$Comp
L CONN_01X05 P2
U 1 1 56C5CEFA
P 6450 2800
F 0 "P2" H 6450 3100 50  0000 C CNN
F 1 "CONN_01X05" V 6550 2800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05" H 6450 2800 60  0001 C CNN
F 3 "" H 6450 2800 60  0000 C CNN
	1    6450 2800
	0    1    1    0   
$EndComp
Wire Wire Line
	6950 1600 6950 1750
Connection ~ 6950 1700
Wire Wire Line
	6750 1600 6750 1950
Connection ~ 6750 1900
Wire Wire Line
	6550 1600 6550 2150
Connection ~ 6550 2100
Wire Wire Line
	6350 1600 6350 2600
Connection ~ 6350 2300
Wire Wire Line
	6150 1600 6150 2600
Connection ~ 6150 2500
Wire Wire Line
	5550 2100 5550 1050
Connection ~ 5550 1050
Wire Wire Line
	6150 2600 6250 2600
Wire Wire Line
	6550 2150 6450 2150
Wire Wire Line
	6450 2150 6450 2600
Wire Wire Line
	6750 1950 6600 1950
Wire Wire Line
	6550 2200 6550 2600
Wire Wire Line
	6950 1750 6800 1750
Wire Wire Line
	6800 1750 6800 2000
Wire Wire Line
	6800 2000 6650 2000
Wire Wire Line
	6650 2000 6650 2600
Wire Wire Line
	6600 2200 6550 2200
Wire Wire Line
	6600 1950 6600 2200
$Comp
L 74HC595POWER U1
U 1 1 56C681CA
P 2850 1700
F 0 "U1" H 3000 2300 70  0000 C CNN
F 1 "74HC595POWER" H 2850 1100 70  0000 C CNN
F 2 "" H 2850 1700 60  0000 C CNN
F 3 "" H 2850 1700 60  0000 C CNN
	1    2850 1700
	0    -1   1    0   
$EndComp
$Comp
L 74HC595POWER U2
U 1 1 56C68EF9
P 4300 1700
F 0 "U2" H 4450 2300 70  0000 C CNN
F 1 "74HC595POWER" H 4300 1100 70  0000 C CNN
F 2 "" H 4300 1700 60  0000 C CNN
F 3 "" H 4300 1700 60  0000 C CNN
	1    4300 1700
	0    -1   1    0   
$EndComp
Text GLabel 3350 1150 2    39   Input ~ 0
GND
Text GLabel 3300 1050 2    39   Input ~ 0
5V
Wire Wire Line
	3300 1250 3300 1150
Wire Wire Line
	3300 1150 3350 1150
Wire Wire Line
	3200 1250 3200 1050
Wire Wire Line
	3200 1050 3300 1050
Text GLabel 4800 1150 2    39   Input ~ 0
GND
Text GLabel 4750 1050 2    39   Input ~ 0
5V
Wire Wire Line
	4750 1250 4750 1150
Wire Wire Line
	4750 1150 4800 1150
Wire Wire Line
	4750 1050 4650 1050
Wire Wire Line
	4650 1050 4650 1250
$EndSCHEMATC
