#ifndef _CSPI_h
#define _CSPI_h
#include <avr/io.h>
#ifdef __cplusplus
extern "C" {
#endif

	void SPI_init();
	void SPI_Transmit(uint16_t data);

#ifdef __cplusplus
}
#endif
#endif

