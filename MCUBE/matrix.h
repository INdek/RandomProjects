// matrix.h
#ifndef _MATRIX_h
#define _MATRIX_h
#include "LED_Colour.h"
#include "CSPI.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#ifdef __cplusplus
extern "C" {
#endif

#define CYCLE_END_MATRIX 2
#define CYCLE_END_LINE 1
#define CYCLE_ONGOING_LINE 0

	extern led_colour matrix[20];

	//define the color of the led
	void matrix_set_colour(uint8_t face, uint8_t led, led_colour colour);
	led_colour matrix_get_colour(uint8_t face, uint8_t led);

	//update the colour of the led
	//Return Values
	//CYCLE_END_MATRIX - The whole matrix has been outputed
	//CYCLE_END_LINE - The current line has been outputed
	//CYCLE_ONGOING_LINE - The current line is still being outputed
	uint8_t matrix_next_cycle();

	//Does a full matrix cycle
	void dump_matrix();

	//turns all matrix led's off
	void paint_matrix(const led_colour z);



#ifdef __cplusplus
}
#endif
#endif

