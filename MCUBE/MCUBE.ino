#include <avr/io.h>
#include <avr/interrupt.h>
#include <CapacitiveSensor.h>

#include "LED_Colour.h"
#include "matrix.h"
#include "CSPI.h"
#include "TEMP_SENS.h"

#define CAP_ACTIVE 2 //current active capacitive sensor
#define CAP_N 5 //Number of capacitive sensors
#define CAP_SAMPLES 8 //Number of samples to take
#define CAP_TIMEOUT 100 //number of milliseconds to wait before a timeout

//initialize all the capacitive sensors
CapacitiveSensor caps[CAP_N] = {
	CapacitiveSensor(A0, A1),
	CapacitiveSensor(A0, A2),
	CapacitiveSensor(A0, A3),
	CapacitiveSensor(A0, A4),
	CapacitiveSensor(A0, A5)
};

//this variable holds the current value of the active cap sensor
static volatile long cap_value = 0;
//value of the temperature sensor
static volatile uint8_t temp_value = 0;

//color table
#define PRESET_SIZE 8
static led_colour cp[PRESET_SIZE] = {
	{0x00, 0x00, 0x00},
	{0x00, 0x00, 0xff},
	{0x00, 0xff, 0x00},
	{0x00, 0xff, 0xff},
	{0xff, 0x00, 0x00},
	{0xff, 0x00, 0xff},
	{0xff, 0xff, 0x00},
	{0xff, 0xff, 0xff}
};

uint8_t interpert_temp_value() {
	return temp_value - TEMP_CALIB_VAL;
}

uint8_t interpert_cap_value() {
	if (cap_value > 512) return 1;
	else return 0;
}

void update_cap() {
	cap_value = caps[CAP_ACTIVE].capacitiveSensor(CAP_SAMPLES);
}

void update_temp() {
	temp_value = temp_read();
}

void setup() {
	//Setup ports PD0:3 as outputs
	DDRD = 0x0F;
	DDRC = 0xFF;

	//set a timeout for the capacitive sensor
	caps[CAP_ACTIVE].set_CS_Timeout_Millis(CAP_TIMEOUT);

	//asm volatile ("nop");
	SPI_init();
	temp_init();
	paint_matrix({ 0x00, 0x00, 0x00 });
}

static uint8_t temp;

void loop() {
	update_cap();

	if (interpert_cap_value()) {
		led_colour color = cp[random(0, PRESET_SIZE)];
		matrix_set_colour(random(0,5), random(0,4), color);

		if (random(0, 1000) < interpert_temp_value()) {
			paint_matrix(cp[random(0, PRESET_SIZE)]);
		}
	}
	
	update_temp();

	for(uint16_t i = 0; i < 300; i++)
		dump_matrix();
}