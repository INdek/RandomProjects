#include <msp430g2553.h>

void setup()
{

  WDTCTL = WDTPW + WDTHOLD; // Stop WDT

  //All buttons are connected to P2
  P2SEL = 0; //I/O Function on all Pins
  P2SEL2 = 0;

  P2DIR &= ~(0b00111111); // Select pins as inputs
  P2REN |= 0b00111111; // Enable Pullup/Pulldown resistor
  P2OUT |= 0b00111111; // Select pins as Pullup

  P2IE |= 0b00111111; // Enable Interrupt
  P2IES |= 0b00111111; // Interrupt on falling edge
  P2IFG &= ~(0b00111111); // Clear all interrupts on Port 2

  P1SEL |= BIT1 | BIT2; // UART function on pin 1.1 and 1.2
  P1SEL2 |= BIT1 | BIT2; // P1.1 = RXD and P1.2 = TXD

  //UART Initialization

  //Calibrate and set DCO at 1MHz
  DCOCTL = 0; // Select lowest DCOx and MODx settings<
  BCSCTL1 = CALBC1_1MHZ; // Set DCO
  DCOCTL = CALDCO_1MHZ;

  //Using ACLK we might be able to go into LPM3
  UCA0CTL1 |= UCSSEL_2; // SMCLK as a clock source
  UCA0BR0 = 0x68; // Prescaler Value (UCAxBR0 + UCAxBR1 * 256) = 104
  UCA0BR1 = 0x00; // 1MHz / 0x68 = 14705
  UCA0MCTL = UCBRS0; // Modulation UCBRSx = 1
					 // Page 424 of the device user guide
					 // Page 420 also usefull
  UCA0CTL1 &= ~UCSWRST; // **Initialize USCI state machine**
  UC0IE |= UCA0RXIE; // Enable USCI_A0 RX interrupt
}

void loop()
{
	//Enter LPM4 with Interrupts enabled
	__bis_SR_register(CPUOFF + SCG0 + SCG1 + OSCOFF + GIE); // Enter LPM4 w/ int
	//Really doesent matter what I do here to save power, the HC-06 is constantly consuming
	//at least 19mA so, saving 500 uA doesen't make any difference
}

#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{
	if (UCA0RXBUF == 22) { //SYN
		UCA0TXBUF = 6; //ACK
	}
}

#pragma vector=USCIAB0TX_VECTOR
__interrupt void USCI0TX_ISR(void)
{
  UCA0TXBUF = '\n'; // TX next character
  UC0IE &= ~UCA0TXIE; // Disable USCI_A0 TX interrupt
}

// Port 2 ISR
#pragma vector=PORT2_VECTOR
__interrupt void P2_ISR(void)
{
	
	switch (P2IFG) { //Send Button Number on interrupt
	case BIT0:
		UCA0TXBUF = '1';
		P2IFG &= ~(BIT0); // Clear Interrupt for this pin
		break;
	case BIT1:
		UCA0TXBUF = '2';
		P2IFG &= ~(BIT1); // Clear Interrupt for this pin
		break;
	case BIT2:
		UCA0TXBUF = '3';
		P2IFG &= ~(BIT2); // Clear Interrupt for this pin
		break;
	case BIT3:
		UCA0TXBUF = '4';
		P2IFG &= ~(BIT3); // Clear Interrupt for this pin
		break;
	case BIT4:
		UCA0TXBUF = '5';
		P2IFG &= ~(BIT4); // Clear Interrupt for this pin
		break;
	case BIT5:
		UCA0TXBUF = '6';
		P2IFG &= ~(BIT5); // Clear Interrupt for this pin
	}
	UC0IE |= UCA0TXIE; // Enable USCI_A0 TX interrupts
	P2IFG = 0;
}
