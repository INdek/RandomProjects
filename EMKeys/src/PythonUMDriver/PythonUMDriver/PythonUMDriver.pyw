﻿#Python User Mode Driver

retry_time = 10 # retry time
keepalive_time = 5 # time to space keepalive packets
keepalive_miss = 3 # number of keepalives we can miss before disconnecting
logging_enabled = False
logfile = "UMDriver.log" # logfile
serial_port = 'COM9' # Bluetooth outgoing com port
                     # Make sure this is reserved for 
                     # the device you are trying to connect to

import serial
import time
import ctypes
from serial import threaded
import sys
import threading
import traceback

##http://stackoverflow.com/questions/13564851/generate-keyboard-events
SendInput = ctypes.windll.user32.SendInput

# C struct redefinitions 
PUL = ctypes.POINTER(ctypes.c_ulong)
class KeyBdInput(ctypes.Structure):
    _fields_ = [("wVk", ctypes.c_ushort),
                ("wScan", ctypes.c_ushort),
                ("dwFlags", ctypes.c_ulong),
                ("time", ctypes.c_ulong),
                ("dwExtraInfo", PUL)]

class HardwareInput(ctypes.Structure):
    _fields_ = [("uMsg", ctypes.c_ulong),
                ("wParamL", ctypes.c_short),
                ("wParamH", ctypes.c_ushort)]

class MouseInput(ctypes.Structure):
    _fields_ = [("dx", ctypes.c_long),
                ("dy", ctypes.c_long),
                ("mouseData", ctypes.c_ulong),
                ("dwFlags", ctypes.c_ulong),
                ("time",ctypes.c_ulong),
                ("dwExtraInfo", PUL)]

class Input_I(ctypes.Union):
    _fields_ = [("ki", KeyBdInput),
                 ("mi", MouseInput),
                 ("hi", HardwareInput)]

class Input(ctypes.Structure):
    _fields_ = [("type", ctypes.c_ulong),
                ("ii", Input_I)]

def PressKey(hexKeyCode):

    extra = ctypes.c_ulong(0)
    ii_ = Input_I()
    ii_.ki = KeyBdInput( hexKeyCode, 0x48, 0, 0, ctypes.pointer(extra) )
    x = Input( ctypes.c_ulong(1), ii_ )
    SendInput(1, ctypes.pointer(x), ctypes.sizeof(x))

def ReleaseKey(hexKeyCode):

    extra = ctypes.c_ulong(0)
    ii_ = Input_I()
    ii_.ki = KeyBdInput( hexKeyCode, 0x48, 0x0002, 0, ctypes.pointer(extra) )
    x = Input( ctypes.c_ulong(1), ii_ )
    SendInput(1, ctypes.pointer(x), ctypes.sizeof(x))

def SimpleKey(hexKeyCode):
    PressKey(hexKeyCode)
    ReleaseKey(hexKeyCode)

def log(text):
    if logging_enabled:
        with open(logfile, "a") as f:
            f.write(text)

VK_VOLUME_DOWN      = 0xAE #1 - Vol Down   - VK_VOLUME_DOWN      - 0xAE
VK_VOLUME_UP        = 0xAF #2 - Vol UP     - VK_VOLUME_UP        - 0xAF
VK_MEDIA_PREV_TRACK = 0xB1 #3 - Prev Track - VK_MEDIA_PREV_TRACK - 0xB1
VK_MEDIA_PLAY_PAUSE = 0xB3 #4 - Play/Pause - VK_MEDIA_PLAY_PAUSE - 0xB3
VK_MEDIA_NEXT_TRACK = 0xB0 #5 - Next Track - VK_MEDIA_NEXT_TRACK - 0xB0
VK_VOLUME_MUTE      = 0xAD #6 - Mute       - VK_VOLUME_MUTE      - 0xAD


class HandleDevice(serial.threaded.LineReader):
    def connection_made(self, transport):
        super(HandleDevice, self).connection_made(transport)
        log('Device Connected\n')

    def data_received(self, data):
        global lastkeepalive

        if data == b'\n':
            return
        if data == b'\x06':
            lastkeepalive = int(time.time())

        if data == b'1':
            SimpleKey(VK_MEDIA_NEXT_TRACK)
        elif data == b'2':
            SimpleKey(VK_VOLUME_DOWN)
        elif data == b'3':
            SimpleKey(VK_VOLUME_UP)
        elif data == b'4':
            SimpleKey(VK_VOLUME_MUTE)
        elif data == b'5':
            SimpleKey(VK_MEDIA_PREV_TRACK)
        elif data == b'6':
            SimpleKey(VK_MEDIA_PLAY_PAUSE)

    def connection_lost(self, exc):
        log('Device Disconnected\n')

def keepalive_func():
    global lastkeepalive
    global connected
    global keepalive_thread

    while True:
        if not ser.isOpen():
            return
        rthread.write(b'\x16')
        time.sleep(keepalive_time)
        keepalive_deadline = lastkeepalive + (keepalive_time * keepalive_miss)
        if int(time.time()) > keepalive_deadline : # if we missed enough keepalive packets
            log("Missed "+str(keepalive_miss)+" KeepAlives disconnecting")
            rthread.close()                        # time to disconnect
            ser.close()
            connected = False
            
lastkeepalive = int(time.time())

while True:
    try:
        ser = serial.Serial(serial_port, 9600, timeout=1)
    except serial.serialutil.SerialException:
        log(traceback.format_exc())
        time.sleep(retry_time)
        continue

    rthread = serial.threaded.ReaderThread(ser, HandleDevice)
    keepalive_thread = threading.Thread(target=keepalive_func)
    rthread.start()
    keepalive_thread.start()
    rthread.join()